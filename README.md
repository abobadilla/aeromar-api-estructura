# Aeromar API Arquitectura

_Arquitectura general para el desarrollo de API's Aeromar_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

```
Java Runtime Environment 1.8
Java Development Kit 1.8 
Archivo: application.properties [Proporcionado por el equipo de desarrollo]
```

### Instalación 🔧

```
Una vez conseguido el archivo 'application.properties' se debera cargar en el directorio 'src/main/resources' para su correcto funcionamiento,
se debera de editar las propiedades internas comenzando con lo siguiente:
* configuration.api.name: Nombre de la aplicación (Sin espacios ni caracteres especiales)
* configuration.api.dir: Directorio global para la carga de archivos de la aplicación (Nunca agregar un directorio interno de la aplicación)
* spring.datasource.url: URL de la base de datos
* spring.datasource.username: Usuario de la base de datos
* spring.datasource.password: Contraseña de la base de datos
* server.port: Puerto por el cual se conectara la aplicación
```

## Construido con 🛠️

_Las herramientas utilizadas para crear y gestionar este proyecto son las siguientes_

* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [Spring Boot](https://spring.io/projects/spring-boot) - Usado como infraestructura
* [MySQL](https://www.mysql.com/) - Manejador de bases de datos

## Versionado 📌

Usamos [GitLab](https://about.gitlab.com/) para el versionado. Para todas las versiones disponibles, mira los [commits en este repositorio](https://gitlab.com/abobadilla/aeromar-api-estructura).



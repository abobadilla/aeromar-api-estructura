/**
 * InternalErrorException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 6 ene. 2020, 18:29:06
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class InternalErrorException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = -5159297966430181713L;

   /**
    * Constructor de la clase.
    * 
    */
   public InternalErrorException() {
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    */
   public InternalErrorException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    * 
    * @param cause
    */
   public InternalErrorException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    */
   public InternalErrorException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public InternalErrorException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

/**
 * InvalidDataException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 6 ene. 2020, 16:33:42
 * @version 1.0.0
 *          <p></p>
 */
public class InvalidDataException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = -105242021760357671L;

   /**
    * Constructor de la clase.
    * 
    */
   public InvalidDataException() {
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    */
   public InvalidDataException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    * 
    * @param cause
    */
   public InvalidDataException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    */
   public InvalidDataException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public InvalidDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

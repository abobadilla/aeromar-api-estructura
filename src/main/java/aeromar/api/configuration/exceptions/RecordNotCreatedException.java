/**
 * RecordNotCreatedException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 18 mar. 2020, 18:09:43
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class RecordNotCreatedException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = 1893887352491311464L;

   /**
    * Constructor de la clase.
    *
    */
   public RecordNotCreatedException() {
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    */
   public RecordNotCreatedException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    *
    * @param cause
    */
   public RecordNotCreatedException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    * @param cause
    */
   public RecordNotCreatedException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public RecordNotCreatedException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

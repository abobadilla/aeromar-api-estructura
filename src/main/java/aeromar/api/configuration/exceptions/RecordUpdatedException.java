/**
 * RecordUpdatedException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 2 abr. 2020, 16:16:23
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class RecordUpdatedException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = -6122623497121432891L;

   /**
    * Constructor de la clase.
    *
    */
   public RecordUpdatedException() {
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    */
   public RecordUpdatedException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    *
    * @param cause
    */
   public RecordUpdatedException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    * @param cause
    */
   public RecordUpdatedException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public RecordUpdatedException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

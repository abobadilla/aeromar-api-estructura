/**
 * ResourceNotFoundException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 13 dic. 2019, 12:05:41
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class ResourceNotFoundException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = -4726139315874753425L;

   /**
    * Constructor de la clase.
    * 
    */
   public ResourceNotFoundException() {
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    */
   public ResourceNotFoundException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    * 
    * @param cause
    */
   public ResourceNotFoundException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    */
   public ResourceNotFoundException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public ResourceNotFoundException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

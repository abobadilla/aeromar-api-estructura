/**
 * ResourceNotImplementedException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 16 ene. 2020, 18:56:57
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class ResourceNotImplementedException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = 8600746076452047113L;

   /**
    * Constructor de la clase.
    * 
    */
   public ResourceNotImplementedException() {
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    */
   public ResourceNotImplementedException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    * 
    * @param cause
    */
   public ResourceNotImplementedException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    */
   public ResourceNotImplementedException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public ResourceNotImplementedException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

/**
 * RecordNotFoundException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 6 ene. 2020, 17:24:08
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class RecordNotFoundException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = 190642308638102570L;

   /**
    * Constructor de la clase.
    * 
    */
   public RecordNotFoundException() {
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    */
   public RecordNotFoundException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    * 
    * @param cause
    */
   public RecordNotFoundException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    */
   public RecordNotFoundException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public RecordNotFoundException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

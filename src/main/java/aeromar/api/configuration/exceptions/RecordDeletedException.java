/**
 * RecordDeletedException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 2 abr. 2020, 17:04:21
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class RecordDeletedException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = -4118969778608912855L;

   /**
    * Constructor de la clase.
    *
    */
   public RecordDeletedException() {
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    */
   public RecordDeletedException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    *
    * @param cause
    */
   public RecordDeletedException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    * @param cause
    */
   public RecordDeletedException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    *
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public RecordDeletedException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

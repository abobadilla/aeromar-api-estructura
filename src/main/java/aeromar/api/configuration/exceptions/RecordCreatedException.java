/**
 * RecordCreatedException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 18 mar. 2020, 10:31:05
 * @version 1.0
 *          <p>
 *          </p>
 *
 */
public class RecordCreatedException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = -4750631402530389154L;

   /**
    * Constructor de la clase.
    * 
    */
   public RecordCreatedException() {
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    */
   public RecordCreatedException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    * 
    * @param cause
    */
   public RecordCreatedException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    */
   public RecordCreatedException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public RecordCreatedException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

/**
 * PartialContentException.java
 */
package aeromar.api.configuration.exceptions;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 20 may. 2020, 21:19:23
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class PartialContentException extends RuntimeException {

   /**
    * 
    */
   private static final long serialVersionUID = -8891772836633457906L;

   /**
    * Constructor de la clase.
    * 
    */
   public PartialContentException() {
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    */
   public PartialContentException(String message) {
      super(message);
   }

   /**
    * Constructor de la clase.
    * 
    * @param cause
    */
   public PartialContentException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    */
   public PartialContentException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructor de la clase.
    * 
    * @param message
    * @param cause
    * @param enableSuppression
    * @param writableStackTrace
    */
   public PartialContentException(String message, Throwable cause, boolean enableSuppression,
         boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }

}

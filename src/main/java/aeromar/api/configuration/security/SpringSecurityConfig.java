/**
 * SpringSecurityConfig.java
 */
package aeromar.api.configuration.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import aeromar.api.configuration.handlers.ApiFilterHandler;
import aeromar.api.configuration.handlers.CustomAuthenticationEntryPointHandler;
import aeromar.api.configuration.handlers.JWTAuthorizationFilterHandler;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import aeromar.api.configuration.handlers.Properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 19 oct. 2020
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringSecurityConfig.class);
	@Autowired
	DataSource dataSource;
    private Properties config;
	
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param config
	 * @param servicioUsuario
	 */
	public SpringSecurityConfig(Properties config) {
		super();
		this.config = config;
	}

	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		/*
		 * 1. Se desactiva el uso de cookies
		 * 2. Se activa la configuración CORS con los valores por defecto
		 * 3. Se desactiva el filtro CSRF
		 * 5. Se indica que el resto de URLs esten securizadas
		*/
		
		httpSecurity
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.cors().and()
		.csrf().disable()
		.authorizeRequests()
			.antMatchers(
				"/Administrador/SolicitarVuelo",
				"/Administrador/PagoReferenciado",
				"/Configurador"
			).permitAll() 
			.anyRequest().authenticated().and()
        .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPointHandler(config)).and()
			.addFilter(new JWTAuthorizationFilterHandler(authenticationManager()));
	}
	
 	@Autowired
 	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
 		auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder());
 	}
	
	
	@Bean(name = "SpringInitializerBean", initMethod = "StartInitialize")
    public SpringInitializerBean initializer(){
        return new SpringInitializerBean();
    }
	
	@Bean
	public FirebaseApp firebaseApp() {
		try {
			FileInputStream serviceAccount = serviceAccount = new FileInputStream(this.config.getFireBaseConf());
			FirebaseOptions options = FirebaseOptions.builder().setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();
			return FirebaseApp.initializeApp(options);
		} catch (IllegalStateException ex) {
			return FirebaseApp.getInstance();
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return null;
	}
	
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}

	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	@Bean
	public JdbcUserDetailsManager jdbcUserDetailsManager() throws Exception {
		JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
		jdbcUserDetailsManager.setDataSource(dataSource);
		return jdbcUserDetailsManager;
	}
	

	
	
}

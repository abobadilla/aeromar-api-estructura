/**
 * SpringInitializerConfig.java
 */
package aeromar.api.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;

import aeromar.api.configuration.handlers.Properties;
import aeromar.api.configuration.helpers.EncryptCBCHelper;
import aeromar.api.configuration.helpers.MailingHelper;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 24 oct. 2020
 * @version 1.0.0
 *          <p>Clase Dedicada a la instancia de propiedades en las clases estáticas
 *          </p>
 */

public class SpringInitializerBean {
	
	@Autowired
    private Properties config;
	
	@Autowired
	private JavaMailSender sender;

	/**
	 *
	 * Descripción: Método de inicio (Carga de parametros)
	 *
	 */
	public void StartInitialize(){
		EncryptCBCHelper.setConfig(config);
		MailingHelper.setConfig(sender,config);
    }
	
	
}

/**
 * GenericResponseObject.java
 */
package aeromar.api.configuration.models;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 22 oct. 2020
 * @version 1.0.0
 *          <p> Clase genérica para Excepciones/Respuestas
 *          </p>
 */

public class GenericResponse<T>{
	private T data;
	
	private HttpStatus status;
	@JsonFormat( pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" )
	private Date timestamp;
	private String message;
	private List<String> details;

	
	

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public GenericResponse() {
		super();
	}






	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param data
	 * @param status
	 * @param timestamp
	 * @param message
	 * @param details
	 */
	public GenericResponse(T data, HttpStatus status, Date timestamp, String message, List<String> details) {
		super();
		this.data = data;
		this.status = status;
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}
	
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param status
	 * @param timestamp
	 * @param message
	 * @param details
	 */
	public GenericResponse(HttpStatus status, Date timestamp, String message, List<String> details) {
		super();
		this.status = status;
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}
	
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param data
	 * @param status
	 * @param timestamp
	 * @param message
	 * @param details
	 */
	public GenericResponse(T data, HttpStatus status, Date timestamp, String message, String details) {
		super();
		this.status = status;
		this.timestamp = timestamp;
		this.message = message;
		this.details = Arrays.asList(details);
	}
	
	
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param status
	 * @param timestamp
	 * @param message
	 * @param details
	 */
	public GenericResponse(HttpStatus status, Date timestamp, String message, String details) {
		super();
		this.status = status;
		this.timestamp = timestamp;
		this.message = message;
		this.details = Arrays.asList(details);
	}
	

	/**
	 * Método para recuperar el valor de la propiedad data
	 *
	 * @return the data
	 */
	public T getData() {
		return data;
	}






	/**
	 * Método para establecer el valor de una propiedad data
	 *
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}






	/**
	 * Método para recuperar el valor de la propiedad status
	 *
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}






	/**
	 * Método para establecer el valor de una propiedad status
	 *
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}






	/**
	 * Método para recuperar el valor de la propiedad timestamp
	 *
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}






	/**
	 * Método para establecer el valor de una propiedad timestamp
	 *
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}






	/**
	 * Método para recuperar el valor de la propiedad message
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}






	/**
	 * Método para establecer el valor de una propiedad message
	 *
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}






	/**
	 * Método para recuperar el valor de la propiedad details
	 *
	 * @return the details
	 */
	public List<String> getDetails() {
		return details;
	}






	/**
	 * Método para establecer el valor de una propiedad details
	 *
	 * @param details the details to set
	 */
	public void setDetails(List<String> details) {
		this.details = details;
	}



	/**
	 *
	 * Descripción: Método para generar una respuesta con estado (OK)
	 *
	 * @param uri
	 * @return
	 */
	public GenericResponse<T> Ok(String uri) {
		if(this.details == null) {
			this.details= new ArrayList<String>();
		}
		this.details.add(uri);
		this.status = HttpStatus.OK;
		this.timestamp= new Date();
		this.message = "Success Request";
		return this;
	}


	/**
	 *
	 * Descripción: Método para generar una respuesta con estado (INTERNAL_SERVER_ERROR)
	 *
	 * @param uri
	 * @return
	 */
	public GenericResponse<T> Fail(String uri) {
		if(this.getDetails() == null) {
			this.setDetails( new ArrayList<String>());
		}
		this.getDetails().add(uri);
		this.setStatus( HttpStatus.INTERNAL_SERVER_ERROR);
		this.setTimestamp( new Date());
		this.setMessage( "Server error, unhandled exception.");
		return this;
	}
	
	/**
	 *
	 * Descripción: Método para generar una respuesta con estado (BAD_REQUEST)
	 *
	 * @param uri
	 * @return
	 */
	public GenericResponse<T> BadRequest(String uri) {
		if(this.getDetails() == null) {
			this.setDetails( new ArrayList<String>());
		}
		this.getDetails().add(uri);
		this.setStatus( HttpStatus.BAD_REQUEST);
		this.setTimestamp(  new Date());
		this.setMessage( "The request that sent to the webserver was malformed");
		return this;
	}






	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GenericResponse [data=");
		builder.append(data);
		builder.append(", status=");
		builder.append(status);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append(", message=");
		builder.append(message);
		builder.append(", details=");
		builder.append(details);
		builder.append("]");
		return builder.toString();
	}
	
	
	


}

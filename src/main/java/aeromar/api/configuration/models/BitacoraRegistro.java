/**
 * BitacoraRegistro.java
 */
package aeromar.api.configuration.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * 
 * @author desarrollo.2 (aeromar.com.mx)
 * @since 7 ene. 2020, 11:18:08
 * @version 1.0.0
 *          <p> Clase genérica que indica los parámetros generales de bitácora en una tabla
 *          </p>
 */
@MappedSuperclass
public abstract class BitacoraRegistro {

   @Column(name = "USR_ID_ALTA", updatable = false)
   private Integer usuarioIdAlta;
   
   @Column(name = "USR_ID_ACT")
   private Integer usuarioIdAct;
   
   @Column(name = "CREATED_DATE", updatable = false)
   private Date createdDate;
   
   @Column(name = "UPDATED_DATE")
   private Date updatedDate;

   /**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public BitacoraRegistro() {
		super();
	}
	
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 */
	public BitacoraRegistro(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate) {
		super();
		this.usuarioIdAlta = usuarioIdAlta;
		this.usuarioIdAct = usuarioIdAct;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}
	
	


	/**
	 * Método para recuperar el valor de la propiedad usuarioIdAlta
	 *
	 * @return the usuarioIdAlta
	 */
	public Integer getUsuarioIdAlta() {
		return usuarioIdAlta;
	}

	/**
	 * Método para establecer el valor de una propiedad usuarioIdAlta
	 *
	 * @param usuarioIdAlta the usuarioIdAlta to set
	 */
	public void setUsuarioIdAlta(Integer usuarioIdAlta) {
		this.usuarioIdAlta = usuarioIdAlta;
	}

	/**
	 * Método para recuperar el valor de la propiedad usuarioIdAct
	 *
	 * @return the usuarioIdAct
	 */
	public Integer getUsuarioIdAct() {
		return usuarioIdAct;
	}

	/**
	 * Método para establecer el valor de una propiedad usuarioIdAct
	 *
	 * @param usuarioIdAct the usuarioIdAct to set
	 */
	public void setUsuarioIdAct(Integer usuarioIdAct) {
		this.usuarioIdAct = usuarioIdAct;
	}

	/**
	 * Método para recuperar el valor de la propiedad createdDate
	 *
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * Método para establecer el valor de una propiedad createdDate
	 *
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Método para recuperar el valor de la propiedad updatedDate
	 *
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * Método para establecer el valor de una propiedad updatedDate
	 *
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BitacoraRegistro [usuarioIdAlta=");
		builder.append(usuarioIdAlta);
		builder.append(", usuarioIdAct=");
		builder.append(usuarioIdAct);
		builder.append(", createdDate=");
		builder.append(createdDate);
		builder.append(", updatedDate=");
		builder.append(updatedDate);
		builder.append("]");
		return builder.toString();
	}

	@PrePersist
    protected void onCreate() {
		this.createdDate = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedDate = new Date();
	}
   
	
   
   
}

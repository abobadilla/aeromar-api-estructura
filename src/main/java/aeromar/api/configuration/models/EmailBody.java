/**
 * EmailBody.java
 */
package aeromar.api.configuration.models;

import java.io.File;
import java.util.Arrays;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 28 oct. 2020
 * @version 1.0.0
 *          <p> Clase genérica que indica los parámetros para el envio de un nuevo Email
 *          </p>
 */
public class EmailBody {
	private String[] sentTo;
	private String[] bcc;
	private String[] cc;
	private String content;
	private String subject;
	private File[] files;
	
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param sentTo
	 * @param bcc
	 * @param cc
	 * @param content
	 * @param subject
	 * @param files
	 */
	public EmailBody(String[] sentTo, String[] bcc, String[] cc, String content, String subject, File[] files) {
		super();
		this.sentTo = sentTo;
		this.bcc = bcc;
		this.cc = cc;
		this.content = content;
		this.subject = subject;
		this.files = files;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public EmailBody() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad sentTo
	 *
	 * @return the sentTo
	 */
	public String[] getSentTo() {
		return sentTo;
	}

	/**
	 * Método para establecer el valor de una propiedad sentTo
	 *
	 * @param sentTo the sentTo to set
	 */
	public void setSentTo(String[] sentTo) {
		this.sentTo = sentTo;
	}

	/**
	 * Método para recuperar el valor de la propiedad bcc
	 *
	 * @return the bcc
	 */
	public String[] getBcc() {
		return bcc;
	}

	/**
	 * Método para establecer el valor de una propiedad bcc
	 *
	 * @param bcc the bcc to set
	 */
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}

	/**
	 * Método para recuperar el valor de la propiedad cc
	 *
	 * @return the cc
	 */
	public String[] getCc() {
		return cc;
	}

	/**
	 * Método para establecer el valor de una propiedad cc
	 *
	 * @param cc the cc to set
	 */
	public void setCc(String[] cc) {
		this.cc = cc;
	}

	/**
	 * Método para recuperar el valor de la propiedad content
	 *
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Método para establecer el valor de una propiedad content
	 *
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Método para recuperar el valor de la propiedad subject
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Método para establecer el valor de una propiedad subject
	 *
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Método para recuperar el valor de la propiedad files
	 *
	 * @return the files
	 */
	public File[] getFiles() {
		return files;
	}

	/**
	 * Método para establecer el valor de una propiedad files
	 *
	 * @param files the files to set
	 */
	public void setFiles(File[] files) {
		this.files = files;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmailBody [sentTo=");
		builder.append(Arrays.toString(sentTo));
		builder.append(", bcc=");
		builder.append(Arrays.toString(bcc));
		builder.append(", cc=");
		builder.append(Arrays.toString(cc));
		builder.append(", content=");
		builder.append(content);
		builder.append(", subject=");
		builder.append(subject);
		builder.append(", files=");
		builder.append(Arrays.toString(files));
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	
	
}

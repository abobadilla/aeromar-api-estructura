/**
 * PropertiesAplication.java
 */
package aeromar.api.configuration.handlers;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 21 oct. 2020
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Component
public class Properties {
	
	@Value("${configuration.api.name}")
	private String apiName;
	
	@Value("${configuration.api.dir}")
	private String apiDir;
	
	@Value("${configuration.api.encrypted}")
	private Boolean apiEncrypted;
	
	@Value("${app.api.logging.enable}")
	private Boolean logEnable;
	
	@Value("${app.api.logging.url_patterns}")
	private String[] logURLPatterns;
	
	@Value("${app.api.logging.requestIdParamName:requestId}")
	private String logRequestIdParamName;
	
	@Value("${app.api.logging.requestIdMDCParamName:REQUEST_ID}")
	private String logRequestIdMDCParamName;
	

	@Value("${configuration.cbc.SECRET_KEY}")
	private String cbcSecretKey;
	
	
	@Value("${spring.mail.username}")
	private String mailUsername;

	@Value("${configuration.firebase.pathConf}")
	private String fireBaseConf;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public Properties() {
		super();
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param apiName
	 * @param apiDir
	 * @param apiEncrypted
	 * @param logEnable
	 * @param logURLPatterns
	 * @param logRequestIdParamName
	 * @param logRequestIdMDCParamName
	 * @param cbcSecretKey
	 * @param mailUsername
	 * @param fireBaseConf
	 */
	public Properties(String apiName, String apiDir, Boolean apiEncrypted, Boolean logEnable, String[] logURLPatterns,
			String logRequestIdParamName, String logRequestIdMDCParamName, String cbcSecretKey, String mailUsername,
			String fireBaseConf) {
		super();
		this.apiName = apiName;
		this.apiDir = apiDir;
		this.apiEncrypted = apiEncrypted;
		this.logEnable = logEnable;
		this.logURLPatterns = logURLPatterns;
		this.logRequestIdParamName = logRequestIdParamName;
		this.logRequestIdMDCParamName = logRequestIdMDCParamName;
		this.cbcSecretKey = cbcSecretKey;
		this.mailUsername = mailUsername;
		this.fireBaseConf = fireBaseConf;
	}

	/**
	 * Método para recuperar el valor de la propiedad apiName
	 *
	 * @return the apiName
	 */
	public String getApiName() {
		return apiName;
	}

	/**
	 * Método para establecer el valor de una propiedad apiName
	 *
	 * @param apiName the apiName to set
	 */
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	/**
	 * Método para recuperar el valor de la propiedad apiDir
	 *
	 * @return the apiDir
	 */
	public String getApiDir() {
		return apiDir;
	}

	/**
	 * Método para establecer el valor de una propiedad apiDir
	 *
	 * @param apiDir the apiDir to set
	 */
	public void setApiDir(String apiDir) {
		this.apiDir = apiDir;
	}

	/**
	 * Método para recuperar el valor de la propiedad apiEncrypted
	 *
	 * @return the apiEncrypted
	 */
	public Boolean getApiEncrypted() {
		return apiEncrypted;
	}

	/**
	 * Método para establecer el valor de una propiedad apiEncrypted
	 *
	 * @param apiEncrypted the apiEncrypted to set
	 */
	public void setApiEncrypted(Boolean apiEncrypted) {
		this.apiEncrypted = apiEncrypted;
	}

	/**
	 * Método para recuperar el valor de la propiedad logEnable
	 *
	 * @return the logEnable
	 */
	public Boolean getLogEnable() {
		return logEnable;
	}

	/**
	 * Método para establecer el valor de una propiedad logEnable
	 *
	 * @param logEnable the logEnable to set
	 */
	public void setLogEnable(Boolean logEnable) {
		this.logEnable = logEnable;
	}

	/**
	 * Método para recuperar el valor de la propiedad logURLPatterns
	 *
	 * @return the logURLPatterns
	 */
	public String[] getLogURLPatterns() {
		return logURLPatterns;
	}

	/**
	 * Método para establecer el valor de una propiedad logURLPatterns
	 *
	 * @param logURLPatterns the logURLPatterns to set
	 */
	public void setLogURLPatterns(String[] logURLPatterns) {
		this.logURLPatterns = logURLPatterns;
	}

	/**
	 * Método para recuperar el valor de la propiedad logRequestIdParamName
	 *
	 * @return the logRequestIdParamName
	 */
	public String getLogRequestIdParamName() {
		return logRequestIdParamName;
	}

	/**
	 * Método para establecer el valor de una propiedad logRequestIdParamName
	 *
	 * @param logRequestIdParamName the logRequestIdParamName to set
	 */
	public void setLogRequestIdParamName(String logRequestIdParamName) {
		this.logRequestIdParamName = logRequestIdParamName;
	}

	/**
	 * Método para recuperar el valor de la propiedad logRequestIdMDCParamName
	 *
	 * @return the logRequestIdMDCParamName
	 */
	public String getLogRequestIdMDCParamName() {
		return logRequestIdMDCParamName;
	}

	/**
	 * Método para establecer el valor de una propiedad logRequestIdMDCParamName
	 *
	 * @param logRequestIdMDCParamName the logRequestIdMDCParamName to set
	 */
	public void setLogRequestIdMDCParamName(String logRequestIdMDCParamName) {
		this.logRequestIdMDCParamName = logRequestIdMDCParamName;
	}

	/**
	 * Método para recuperar el valor de la propiedad cbcSecretKey
	 *
	 * @return the cbcSecretKey
	 */
	public String getCbcSecretKey() {
		return cbcSecretKey;
	}

	/**
	 * Método para establecer el valor de una propiedad cbcSecretKey
	 *
	 * @param cbcSecretKey the cbcSecretKey to set
	 */
	public void setCbcSecretKey(String cbcSecretKey) {
		this.cbcSecretKey = cbcSecretKey;
	}

	/**
	 * Método para recuperar el valor de la propiedad mailUsername
	 *
	 * @return the mailUsername
	 */
	public String getMailUsername() {
		return mailUsername;
	}

	/**
	 * Método para establecer el valor de una propiedad mailUsername
	 *
	 * @param mailUsername the mailUsername to set
	 */
	public void setMailUsername(String mailUsername) {
		this.mailUsername = mailUsername;
	}

	/**
	 * Método para recuperar el valor de la propiedad fireBaseConf
	 *
	 * @return the fireBaseConf
	 */
	public String getFireBaseConf() {
		return fireBaseConf;
	}

	/**
	 * Método para establecer el valor de una propiedad fireBaseConf
	 *
	 * @param fireBaseConf the fireBaseConf to set
	 */
	public void setFireBaseConf(String fireBaseConf) {
		this.fireBaseConf = fireBaseConf;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Properties [apiName=");
		builder.append(apiName);
		builder.append(", apiDir=");
		builder.append(apiDir);
		builder.append(", apiEncrypted=");
		builder.append(apiEncrypted);
		builder.append(", logEnable=");
		builder.append(logEnable);
		builder.append(", logURLPatterns=");
		builder.append(Arrays.toString(logURLPatterns));
		builder.append(", logRequestIdParamName=");
		builder.append(logRequestIdParamName);
		builder.append(", logRequestIdMDCParamName=");
		builder.append(logRequestIdMDCParamName);
		builder.append(", cbcSecretKey=");
		builder.append(cbcSecretKey);
		builder.append(", mailUsername=");
		builder.append(mailUsername);
		builder.append(", fireBaseConf=");
		builder.append(fireBaseConf);
		builder.append("]");
		return builder.toString();
	}
	
	
}

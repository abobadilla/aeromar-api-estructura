/**
 * ApiLoggingFilter.java
 */
package aeromar.api.configuration.handlers;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.jboss.logging.MDC;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import aeromar.api.configuration.handlers.wrappers.BufferedRequestWrapper;
import aeromar.api.configuration.handlers.wrappers.BufferedServletResponseWrapper;
import aeromar.api.configuration.handlers.wrappers.PrettyFacesRequestWrapper;
import aeromar.api.configuration.helpers.EncryptCBCHelper;
import aeromar.api.configuration.models.GenericResponse;


/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 21 oct. 2020
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Order(1)
@Component
public class ApiFilterHandler implements Filter   {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApiFilterHandler.class);
	private String APPLICATION_TEXT_PLAIN_WITH_UTF8_CHARSET = MediaType.TEXT_PLAIN + ";charset=" + java.nio.charset.StandardCharsets.UTF_8;
	private String APPLICATION_JSON_WITH_UTF8_CHARSET = MediaType.APPLICATION_JSON + ";charset=" + java.nio.charset.StandardCharsets.UTF_8;
	
	@Autowired
	private Properties propertiesL;
	


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response); 
		/*
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		BufferedRequestWrapper bufferedRequest = new BufferedRequestWrapper(httpServletRequest);
		BufferedRequestWrapper requestFinal = null;
		Boolean badRequest = false;
		Boolean emptyRequestGet = false;
		Boolean emptyRequestPOST = false;
		
		String remoteAddr = "";

        if (request != null) {
            remoteAddr = httpServletRequest.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
	
		// Cabeceras Response
		Boolean generatedException = false;
		HttpServletResponse r = (HttpServletResponse) response;
        HttpServletResponse httpServletResponse =  new HttpServletResponseWrapper(r) 
        {
            @Override
            public ServletOutputStream getOutputStream() throws java.io.IOException
            {
            	ServletResponse response = this.getResponse();
            	if(propertiesL.getApiEncrypted()) {
                    response.setContentType(APPLICATION_TEXT_PLAIN_WITH_UTF8_CHARSET);
            	}else if(response.getContentType() == null) {
            		response.setContentType(APPLICATION_JSON_WITH_UTF8_CHARSET);
            	}
                return super.getOutputStream();
            }
        };
        BufferedServletResponseWrapper bufferedResponse = new BufferedServletResponseWrapper(httpServletResponse);
        
        // Se genera un identificador unico para cada solicitud
        Map<String, String> requestMap = this.getTypesafeRequestMap(httpServletRequest);
        String requestId = requestMap.containsKey(propertiesL.getLogRequestIdParamName()) ? requestMap.get(propertiesL.getLogRequestIdParamName()): UUID.randomUUID().toString();
		MDC.put(propertiesL.getLogRequestIdMDCParamName(), " "+requestId);
		
		
		// Se desencripta la solicitud
		JSONObject urlParams = new JSONObject();
		JSONObject bodyParams = new JSONObject();
		if(propertiesL.getApiEncrypted()) {
			
			// Obtener valores Parametros URL
			JSONObject getParamsCrypted=  this.getTypesafeRequestJson(httpServletRequest);
			if(!getParamsCrypted.isEmpty()) {
				if(getParamsCrypted.has("data") ) {
					if(getParamsCrypted.length() == 1 && getParamsCrypted.get("data").toString().length()> 0) {
						try {
							String result = java.net.URLDecoder.decode(getParamsCrypted.getString("data"), StandardCharsets.UTF_8.name());
							urlParams = new JSONObject(EncryptCBCHelper.decryptCBC(result.replace(" ", "+")));
							Map<String, String[]> requestMapURL = getTypesafeRequestMapFromJson(urlParams);
					    	PrettyFacesRequestWrapper extra = new PrettyFacesRequestWrapper(httpServletRequest, requestMapURL);
					    	requestFinal = new BufferedRequestWrapper(extra);
						} catch (Exception e) {
							badRequest = true;
							LOGGER.error(e.getMessage());
						}
					}else {
						badRequest = true;
					}
				}else {
					badRequest =true;
				}
			}else {
				emptyRequestGet =true;
			}
			
			// Obtener valores Parametros BODY Request
			if(bufferedRequest.getRequestBody().length()> 0) {
				JSONObject postParamsCrypted= new JSONObject(bufferedRequest.getRequestBody());
				if(postParamsCrypted.has("data")) {
					if(postParamsCrypted.length() == 1 && postParamsCrypted.get("data").toString().length()> 0) {
						try {
							bodyParams = new JSONObject(EncryptCBCHelper.decryptCBC(postParamsCrypted.getString("data").replace(" ", "+")));
							InputStream is = new ByteArrayInputStream(bodyParams.toString().getBytes());
					    	requestFinal = new BufferedRequestWrapper(httpServletRequest,is);
						} catch (Exception e) {
							badRequest = true;
							LOGGER.error(e.getMessage());
						}
					}else {
						badRequest = true;
					}
				}else {
					badRequest= true;
				}
			}else {
				emptyRequestPOST= true;
			}
			
			if(emptyRequestPOST && emptyRequestGet) {
				// Peticion Vacia
				Map<String, String[]> requestMapURL = new HashMap<String, String[]>();
		    	PrettyFacesRequestWrapper extra = new PrettyFacesRequestWrapper(httpServletRequest, requestMapURL);
		    	requestFinal = new BufferedRequestWrapper(extra);
			}
			
	    	// Generar Log de Entrada
			StringBuilder logRequest = new StringBuilder("HTTP ").append(httpServletRequest.getMethod()).append(" \"").append(httpServletRequest.getServletPath()).append("\" ").append(", parameters=").append(urlParams).append(", body=").append(bodyParams).append(", remote_address=").append(remoteAddr).append(", token= ").append(MDC.get("user-data")== null ?  "{}" : MDC.get("user-data") );
			LOGGER.info(logRequest.toString());
		}else {
			// Generar Log de Entrada
			StringBuilder logRequest = new StringBuilder("HTTP ").append(httpServletRequest.getMethod()).append(" \"").append(httpServletRequest.getServletPath()).append("\" ").append(", parameters=").append(requestMap).append(", body=").append(bufferedRequest.getRequestBody()).append(", remote_address=").append(remoteAddr).append(", token= ").append(MDC.get("user-data")== null ?  "{}" : MDC.get("user-data") );
			LOGGER.info(logRequest.toString());
			
			// En caso de que el sistema no este encriptado, pasar datos normales
			requestFinal = bufferedRequest;
		}
		
		String outResponse = new String();
		if(!badRequest) {
			try {
	        	chain.doFilter(requestFinal, bufferedResponse);
	        }catch (Throwable a) {
				generatedException= true;
				LOGGER.error(a.getMessage());
			}finally {
				if(generatedException ) {
					// Generar respuesta global (Respuesta Equivocada)
					GenericResponse<Object> errorResponse = new GenericResponse<Object>();
					
					ObjectMapper om = new ObjectMapper();
		            String jsonString = om.writeValueAsString(errorResponse.BadRequest("uri" + ": " + httpServletRequest.getRequestURI()));
					
		            outResponse= jsonString;
					
					final StringBuilder logResponse = new StringBuilder("HTTP RESPONSE ").append(outResponse);
					r.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
					LOGGER.info(logResponse.toString());
				}else {
					// Generar log de respuesta Exitosa
					StringBuilder logResponse = new StringBuilder("HTTP RESPONSE ").append(bufferedResponse.getResponseData());
					outResponse = bufferedResponse.getResponseData();
					LOGGER.info(logResponse.toString());
				}
	            
	        }
		}else {
			GenericResponse<Object> errorResponse = new GenericResponse<Object>();
			ObjectMapper om = new ObjectMapper();
            String jsonString = om.writeValueAsString(errorResponse.BadRequest("uri" + ": " + httpServletRequest.getRequestURI()));
			outResponse= jsonString;
			final StringBuilder logResponse = new StringBuilder("HTTP RESPONSE ").append(outResponse);
			r.setStatus(HttpStatus.BAD_REQUEST.value());
			LOGGER.info(logResponse.toString());
		}
		
		// Encriptado de Respuesta
		if(propertiesL.getApiEncrypted()) {
        	try {
        		outResponse = EncryptCBCHelper.encryptCBC(outResponse);
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				outResponse = "Error";
				r.setStatus(HttpStatus.PRECONDITION_FAILED.value());
			}
		}
		
		String encryptedResponseData = outResponse;
        OutputStream outputStream = httpServletResponse.getOutputStream();
        outputStream.write(encryptedResponseData.getBytes());
        outputStream.flush();
        outputStream.close();
        MDC.clear();*/
	}
	
	

	private JSONObject getTypesafeRequestJson(HttpServletRequest request) {
		Enumeration<?> requestParamNames = request.getParameterNames();
		JSONObject newJsonObject = new JSONObject();
		while (requestParamNames.hasMoreElements()) {
			String requestParamName = (String) requestParamNames.nextElement();
			String requestParamValue = request.getParameter(requestParamName);
			newJsonObject.put(requestParamName, requestParamValue);
		}
		return newJsonObject;
	}
	
	private Map<String, String> getTypesafeRequestMap(HttpServletRequest request) {
		Map<String, String> typesafeRequestMap = new HashMap<String, String>();
		Enumeration<?> requestParamNames = request.getParameterNames();
		while (requestParamNames.hasMoreElements()) {
			String requestParamName = (String) requestParamNames.nextElement();
			String requestParamValue;
			requestParamValue = request.getParameter(requestParamName);
			typesafeRequestMap.put(requestParamName, requestParamValue);
		}
		return typesafeRequestMap;
	}
	
	private Map<String, String[]> getTypesafeRequestMapFromJson(JSONObject request) {
		Map<String, String[]> typesafeRequestMap = new HashMap<String, String[]>();
		Iterator<String> requestParamNames = request.keys();
		while (requestParamNames.hasNext()) {
			String requestParamName = (String) requestParamNames.next();
			String requestParamValue;
			requestParamValue = request.get(requestParamName).toString();
			typesafeRequestMap.put(requestParamName,  new String[]{requestParamValue});
		}
		return typesafeRequestMap;
	}


	
}
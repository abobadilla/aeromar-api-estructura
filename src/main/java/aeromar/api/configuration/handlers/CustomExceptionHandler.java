/**
 * CustomExceptionHandler.java
 */
package aeromar.api.configuration.handlers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import aeromar.api.configuration.exceptions.InternalErrorException;
import aeromar.api.configuration.exceptions.InvalidDataException;
import aeromar.api.configuration.exceptions.PartialContentException;
import aeromar.api.configuration.exceptions.RecordCreatedException;
import aeromar.api.configuration.exceptions.RecordDeletedException;
import aeromar.api.configuration.exceptions.RecordNotCreatedException;
import aeromar.api.configuration.exceptions.RecordNotFoundException;
import aeromar.api.configuration.exceptions.RecordUpdatedException;
import aeromar.api.configuration.exceptions.ResourceNotFoundException;
import aeromar.api.configuration.exceptions.ResourceNotImplementedException;
import aeromar.api.configuration.models.GenericResponse;
import javassist.NotFoundException;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 19 oct. 2020
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	
   /**
    * Constructor de la clase.
    * 
    */
	
   public CustomExceptionHandler() {
   }
   
   
 

   @Override
   protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
      List<String> errors = new ArrayList<String>();
      for (FieldError error : exception.getBindingResult().getFieldErrors()) {
         errors.add(error.getField() + ": " + error.getDefaultMessage());
      }
      for (ObjectError error : exception.getBindingResult().getGlobalErrors()) {
         errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
      }
      GenericResponse<Object> details = new GenericResponse<Object>(HttpStatus.BAD_REQUEST, new Date(), exception.getLocalizedMessage(), errors);
      return handleExceptionInternal(exception, details, headers, details.getStatus(), request);
   }

   @ExceptionHandler(NotFoundException.class)
   public final ResponseEntity<GenericResponse<Object>> handleResourceNotFoundIntException(NotFoundException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.NOT_FOUND, new Date(), exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
   }

   
   @ExceptionHandler(ResourceNotFoundException.class)
   public final ResponseEntity<GenericResponse<Object>> handleResourceNotFoundException(ResourceNotFoundException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.EXPECTATION_FAILED, new Date(),exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.EXPECTATION_FAILED);
   }

   @ExceptionHandler(ResourceNotImplementedException.class)
   public final ResponseEntity<GenericResponse<Object>> handleResourceNotImplementedException(ResourceNotImplementedException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.NOT_IMPLEMENTED, new Date(),exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.NOT_IMPLEMENTED);
   }

   @ExceptionHandler(InvalidDataException.class)
   public final ResponseEntity<GenericResponse<Object>> handleInvalidDataException(InvalidDataException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.BAD_REQUEST, new Date(), exception.getMessage(),request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
   }

   @ExceptionHandler(RecordNotFoundException.class)
   public final ResponseEntity<GenericResponse<Object>> handleRecordNotFoundException(RecordNotFoundException exception,WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.NOT_FOUND, new Date(), exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
   }

   @ExceptionHandler(InternalErrorException.class)
   public final ResponseEntity<GenericResponse<Object>> handleInternalErrorException(InternalErrorException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.INTERNAL_SERVER_ERROR, new Date(), exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
   }

   @ExceptionHandler(RecordNotCreatedException.class)
   public final ResponseEntity<GenericResponse<Object>> handleRecordNotCreatedException(RecordNotCreatedException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.INTERNAL_SERVER_ERROR, new Date(), exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
   }

   @ExceptionHandler(RecordCreatedException.class)
   public final ResponseEntity<GenericResponse<Object>> handleRecordCreatedException(RecordCreatedException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.CREATED, new Date(), exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.CREATED);
   }

   @ExceptionHandler(RecordUpdatedException.class)
   public final ResponseEntity<GenericResponse<Object>> handleRecordUpdatedException(RecordUpdatedException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.OK, new Date(), exception.getMessage(),request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.OK);
   }

   @ExceptionHandler(RecordDeletedException.class)
   public final ResponseEntity<GenericResponse<Object>> handleRecordDeletedException(RecordDeletedException exception, WebRequest request) {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.OK, new Date(), exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.OK);
   }
   
   
   
   @ExceptionHandler(PartialContentException.class)
   public final ResponseEntity<GenericResponse<Object>> handlePartialContentException(PartialContentException exception, WebRequest request) {
	   
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.PARTIAL_CONTENT, new Date(), exception.getMessage(), request.getDescription(false));
      return new ResponseEntity<>(errorDetails, HttpStatus.PARTIAL_CONTENT);
   }

  
   // Excepciones Internas 
   @Override
   protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,HttpHeaders headers,HttpStatus status,WebRequest request)
   {
	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(status, new Date(), ex.getMessage(), request.getDescription(false));
	   return new ResponseEntity<>(errorDetails, status);
   }
   
   @Override
	protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(status, new Date(), ex.getMessage(), request.getDescription(false));
	   return new ResponseEntity<>(errorDetails, status);
	}

   @Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(status, new Date(), ex.getMessage(), request.getDescription(false));
	   return new ResponseEntity<>(errorDetails, status);
	}
   

   @Override
	protected ResponseEntity<Object> handleAsyncRequestTimeoutException(
			AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {

	   GenericResponse<Object> errorDetails = new GenericResponse<Object>(status, new Date(), ex.getMessage(), webRequest.getDescription(false));
	   return new ResponseEntity<>(errorDetails, status);
	}

   @Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
		}
		GenericResponse<Object> errorDetails = new GenericResponse<Object>(status, new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, status);
	}
   
   
}
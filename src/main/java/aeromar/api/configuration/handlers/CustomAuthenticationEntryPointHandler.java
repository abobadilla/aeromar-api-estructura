/**
 * CustomAuthenticationEntryPoint.java
 */
package aeromar.api.configuration.handlers;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.fasterxml.jackson.databind.ObjectMapper;

import aeromar.api.configuration.helpers.EncryptCBCHelper;
import aeromar.api.configuration.models.GenericResponse;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 27 oct. 2020
 * @version 1.0.0
 *          <p>
 *          </p>
 */

public class CustomAuthenticationEntryPointHandler implements AuthenticationEntryPoint {
	
	private Properties propertiesL;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiFilterHandler.class);
	
	private String APPLICATION_TEXT_PLAIN_WITH_UTF8_CHARSET = MediaType.TEXT_PLAIN + ";charset=" + java.nio.charset.StandardCharsets.UTF_8;
	private String APPLICATION_JSON_WITH_UTF8_CHARSET = MediaType.APPLICATION_JSON + ";charset=" + java.nio.charset.StandardCharsets.UTF_8;
	
	public CustomAuthenticationEntryPointHandler(Properties properties) {
		this.propertiesL= properties;
	}
	
    @Override
    public void commence(HttpServletRequest req, HttpServletResponse res, AuthenticationException authException) throws IOException, ServletException {
    	EncryptCBCHelper.setConfig(propertiesL);
    	if(propertiesL.getApiEncrypted()) {
    		res.setContentType(APPLICATION_TEXT_PLAIN_WITH_UTF8_CHARSET);
            res.setStatus(HttpStatus.UNAUTHORIZED.value());
            GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.UNAUTHORIZED, new Date(), authException.getMessage(), req.getRequestURI());
            
            ObjectMapper om = new ObjectMapper();
            String jsonString = om.writeValueAsString(errorDetails);
            try {
				res.getWriter().write(EncryptCBCHelper.encryptCBC(jsonString));
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
			}
    	}else {
    		res.setContentType(APPLICATION_JSON_WITH_UTF8_CHARSET);
            res.setStatus(HttpStatus.UNAUTHORIZED.value());
            GenericResponse<Object> errorDetails = new GenericResponse<Object>(HttpStatus.UNAUTHORIZED, new Date(), authException.getMessage(), req.getRequestURI());

            ObjectMapper om = new ObjectMapper();
            String jsonString = om.writeValueAsString(errorDetails);
            res.getWriter().write(jsonString);
    	}
        
    }
}
/**
 * JWTAuthorizationFilter.java
 */
package aeromar.api.configuration.handlers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.MDC;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;


/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 19 oct. 2020
 * @version 1.0.0
 *          <p>
 *          </p>
 */

public class JWTAuthorizationFilterHandler extends BasicAuthenticationFilter {
	private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthorizationFilterHandler.class);

    public JWTAuthorizationFilterHandler(AuthenticationManager authManager) {
		super(authManager);
	}

    /**
	 *
	 * Descripción: Filtro que verifica el JWT
	 *
	 * @param request
	 * @return
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
		String header = req.getHeader("Authorization");
		if (header == null || !header.startsWith("Bearer ")) {
			chain.doFilter(req, res);
			return;
		}
		
		UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}

	/**
	 *
	 * Descripción: Método creado para la extracción del JWT y la asignación del objeto Authentication
	 *
	 * @param request
	 * @return
	 */
	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		try {
			FirebaseToken decodedToken = decodedToken = FirebaseAuth.getInstance().verifyIdToken(token.replace("Bearer ", ""));
			//if(decodedToken.isEmailVerified()) {
				JSONObject tokenDecF= new JSONObject(decodedToken);
				MDC.put("user-data", tokenDecF);
				return new UsernamePasswordAuthenticationToken(tokenDecF, null, new ArrayList<>());
			//}
			
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		return null;
	}
}
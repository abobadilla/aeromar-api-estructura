/**
 * EncryptCBCHelper.java
 */
package aeromar.api.configuration.helpers;

import java.security.MessageDigest;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;

import aeromar.api.configuration.handlers.Properties;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 23 oct. 2020
 * @version 1.0.0
 *          <p>
 *          	Encriptación por AES/CBC (CBC)
 *          </p>
 */


public class EncryptCBCHelper {
	
	private static Properties config;
	
	/**
	 *
	 * Descripción: Método dedicado a encriptar (CBC) un texto plano
	 *
	 * @param plainText
	 * @return String
	 * @throws Exception
	 */
	public static String encryptCBC(String plainText) throws Exception {
		IvParameterSpec iv = new IvParameterSpec(new byte[16]);
        SecretKeySpec skeySpec = new SecretKeySpec(config.getCbcSecretKey().getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(plainText.getBytes());
        return Base64.encodeBase64String(encrypted);
    }

	/**
	 *
	 * Descripción: Método dedicado a desencriptar (CBC) un texto plano
	 *
	 * @param encryptedR
	 * @return String
	 * @throws Exception
	 */
	public static String decryptCBC(String encryptedR) throws Exception {
		IvParameterSpec iv = new IvParameterSpec(new byte[16]);
        SecretKeySpec skeySpec = new SecretKeySpec(config.getCbcSecretKey().getBytes("UTF-8"), "AES");
 
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        byte[] original = cipher.doFinal(Base64.decodeBase64(encryptedR));
 
        return new String(original);
    }

	/**
	 * Método para establecer el valor de una propiedad config
	 *
	 * @param config the config to set
	 */
	public static void setConfig(Properties config) {
		EncryptCBCHelper.config = config;
	}
	
	
}

/**
 * EmailService.java
 */
package aeromar.api.configuration.helpers;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import aeromar.api.configuration.handlers.Properties;
import aeromar.api.configuration.models.EmailBody;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 28 oct. 2020
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Service
public class MailingHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(MailingHelper.class);
	private static JavaMailSender sender;
	private static Properties config;



	public static boolean sendEmailTool(EmailBody emailBody) {
		boolean send = false;
		boolean multipart = false;
		MimeMessage message = sender.createMimeMessage();
		if(emailBody.getFiles() != null && emailBody.getFiles().length > 0) {
			multipart = true;
		}
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, multipart);
			helper.setFrom(MailingHelper.config.getMailUsername());
			helper.setTo(emailBody.getSentTo());
			
			if(emailBody.getBcc() != null) {
				helper.setBcc(emailBody.getBcc());
			}
			
			if(emailBody.getCc() != null) {
				helper.setCc(emailBody.getCc());
			}
			
			helper.setText(emailBody.getContent(), true);
			helper.setSubject(emailBody.getSubject());
			
			if(emailBody.getFiles() != null && emailBody.getFiles().length > 0) {
				for (File file : emailBody.getFiles()) {
					helper.addAttachment(file.getName(), file);
		        }
			}
			sender.send(message);
			send = true;
			LOGGER.info("Mail sent successfully");
		} catch (MessagingException e) {
			LOGGER.error("Hubo un error al enviar el correo: "+ e.getMessage());
			send= false;
		}catch (Exception e) {
			LOGGER.error("Hubo un error interno al enviar el correo: "+ e.getMessage());
			send= false;
		}
		return send;
	}


	/**
	 * Método para establecer el valor de una propiedad JavaMailSender y Properties
	 *
	 * @param sender the sender to set
	 */
	public static void setConfig(JavaMailSender sender, Properties config) {
		MailingHelper.sender = sender;
		MailingHelper.config= config;
	}
	
	
}

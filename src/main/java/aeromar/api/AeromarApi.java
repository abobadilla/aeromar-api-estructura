package aeromar.api;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class AeromarApi {
	public static void main(String[] args) {
		SpringApplication.run(AeromarApi.class, args);

	}
	
}

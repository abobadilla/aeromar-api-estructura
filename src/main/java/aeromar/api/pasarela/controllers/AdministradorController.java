package aeromar.api.pasarela.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import aeromar.api.configuration.models.GenericResponse;
import aeromar.api.pasarela.models.Header;
import aeromar.api.pasarela.models.RequestPagoReferenciado;
import aeromar.api.pasarela.services.AdministradorService;

@RestController
@RequestMapping("/Administrador")
@CrossOrigin(origins = { "*" }, maxAge = 360)
public class AdministradorController {

	@Autowired
	private AdministradorService administrador;
	
	/**
	 *
	 * Descripción: Metodo privado para generar una nueva solicitud de vuelo
	 *
	 * @param solicitud
	 * @return GenericResponse<Header>
	 */
	@RequestMapping(value = "/SolicitarVuelo", method = RequestMethod.POST)
	public GenericResponse<Header> solicitarVuelo(@RequestBody Header solicitud) {
	    GenericResponse<Header> respuesta = new GenericResponse<Header>();
	    respuesta.setData(administrador.generarSolicitudVuelo(solicitud));
	    return respuesta.Ok("/Administrador/SolicitarVuelo");
	}
	
	
	
	/**
	 *
	 * Descripción: Metodo privado para hacer pago referenciado
	 *
	 * @param solicitud
	 * @return GenericResponse<Header>
	 */
	@RequestMapping(value = "/PagoReferenciado", method = RequestMethod.POST)
	public GenericResponse<String> pagoReferenciado(@RequestBody RequestPagoReferenciado request) {
	    GenericResponse<String> respuesta = new GenericResponse<String>();
	    respuesta.setData(administrador.pagoReferenciado( request.getSolicitud(), request.getAfiliacion() ));
	    return respuesta.Ok("/Administrador/PagoReferenciado");
	}
	
	
	
	
	
}
 
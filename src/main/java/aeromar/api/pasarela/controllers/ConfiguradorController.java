package aeromar.api.pasarela.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import aeromar.api.configuration.models.GenericResponse;
import aeromar.api.pasarela.models.ConfiguradorRespuesta;
import aeromar.api.pasarela.models.RequestConfigurador;
import aeromar.api.pasarela.services.ConfiguradorService;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 7 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@RestController
@RequestMapping("/Configurador")
@CrossOrigin(origins = { "*" }, maxAge = 360)
public class ConfiguradorController {

	
	@Autowired
	private ConfiguradorService configurador;
	
	/**
	 *
	 * Descripción: Metodo privado para obtener los detalles del configurador
	 *
	 * @param solicitud
	 * @return GenericResponse<Header>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public GenericResponse<ConfiguradorRespuesta> obtenerDatos(@RequestBody RequestConfigurador rConf) {
	    GenericResponse<ConfiguradorRespuesta> respuesta = new GenericResponse<ConfiguradorRespuesta>();
	    respuesta.setData(configurador.obtenerDatos(rConf));
	    return respuesta.Ok("/Configurador");
	}
	
}

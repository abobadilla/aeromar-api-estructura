package aeromar.api.pasarela.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.CAT_SO_FP;
import aeromar.api.pasarela.models.CAT_SistemaOrigen;

public interface CAT_SO_FPRepository extends JpaRepository<CAT_SO_FP, Integer>{
	public List<CAT_SO_FP> findByFkSistemaOrigen(CAT_SistemaOrigen sistema);
	
	
}

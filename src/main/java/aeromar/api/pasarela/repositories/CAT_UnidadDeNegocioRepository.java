package aeromar.api.pasarela.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.CAT_UnidadDeNegocio;

public interface CAT_UnidadDeNegocioRepository  extends JpaRepository<CAT_UnidadDeNegocio, Integer>{
	public Optional<CAT_UnidadDeNegocio> findByClaveAndEstado(String clave, Boolean estado);
}

package aeromar.api.pasarela.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.DetalleHeader;

public interface DetalleHeaderRepository extends JpaRepository<DetalleHeader, Integer>{

}

package aeromar.api.pasarela.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.ValorEtiqueta;

public interface ValorEtiquetaRepository extends JpaRepository<ValorEtiqueta, Integer>{
	public Optional<ValorEtiqueta> findByFkIdEtiquetaClaveEtiquetaAndValorAndEstatus(String etiqueta, String valor, Boolean estado);
	public Optional<ValorEtiqueta> findByFkIdEtiquetaClaveEtiquetaAndEstatus(String etiqueta, Boolean estado);
}

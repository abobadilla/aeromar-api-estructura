package aeromar.api.pasarela.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.CAT_SistemaOrigen;


public interface CAT_SistemaOrigenRepository  extends JpaRepository<CAT_SistemaOrigen, Integer>{
	public Optional<CAT_SistemaOrigen> findByClaveAndEstado(String clave, Boolean estado);
}

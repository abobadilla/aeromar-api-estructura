package aeromar.api.pasarela.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.AgrupadorDetalle;

public interface AgrupadorDetalleRepository extends JpaRepository<AgrupadorDetalle, Integer>{

}

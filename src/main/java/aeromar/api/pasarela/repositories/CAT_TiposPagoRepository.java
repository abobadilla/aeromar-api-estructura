package aeromar.api.pasarela.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.CAT_TiposPago;

public interface CAT_TiposPagoRepository extends JpaRepository<CAT_TiposPago, Integer>{
	
	public Optional<CAT_TiposPago> findByClaveAndEstado(String clave, Boolean estado);
}

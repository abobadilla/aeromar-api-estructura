package aeromar.api.pasarela.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.CAT_TipoAncillarie;

public interface CAT_TipoAncillarieRepository extends JpaRepository<CAT_TipoAncillarie, Integer>{
	public Optional<CAT_TipoAncillarie> findByClaveAndEstado(String clave, Boolean estado);
}

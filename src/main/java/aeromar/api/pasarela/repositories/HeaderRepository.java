package aeromar.api.pasarela.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.Header;

public interface HeaderRepository extends JpaRepository<Header, Integer>{

}

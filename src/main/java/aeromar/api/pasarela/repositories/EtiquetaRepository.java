package aeromar.api.pasarela.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import aeromar.api.pasarela.models.Etiquetas;

public interface EtiquetaRepository extends JpaRepository<Etiquetas, Integer>{
	
	
	public Optional<Etiquetas> findByClaveEtiquetaAndTipoEtiquetaAndEstatus(String claveEtiqueta, String tipoEtiqueta , Boolean estatus);
	
	
}

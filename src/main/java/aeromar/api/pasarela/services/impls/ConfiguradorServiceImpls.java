package aeromar.api.pasarela.services.impls;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aeromar.api.configuration.exceptions.InvalidDataException;
import aeromar.api.pasarela.models.CAT_SO_FP;
import aeromar.api.pasarela.models.CAT_SistemaOrigen;
import aeromar.api.pasarela.models.CAT_TiposPago;
import aeromar.api.pasarela.models.ConfiguradorRespuesta;
import aeromar.api.pasarela.models.RequestConfigurador;
import aeromar.api.pasarela.repositories.CAT_SO_FPRepository;
import aeromar.api.pasarela.repositories.CAT_SistemaOrigenRepository;
import aeromar.api.pasarela.services.ConfiguradorService;

@Service 
public class ConfiguradorServiceImpls implements ConfiguradorService{

	@Autowired
	private CAT_SO_FPRepository soFPRepository;

	@Autowired
	private CAT_SistemaOrigenRepository sistemasOrigen;
	
	/**
	 *
	 * Descripción: Obtener detalles del configurador
	 *
	 * @return
	 * @see aeromar.api.pasarela.services.ConfiguradorService#obtenerDatos()
	 */
	@Override
	public ConfiguradorRespuesta obtenerDatos(RequestConfigurador rConf) {
		ConfiguradorRespuesta respuesta = new ConfiguradorRespuesta();
		
		Optional<CAT_SistemaOrigen> sistemOrigen = sistemasOrigen.findByClaveAndEstado(rConf.getClaveSO(), true);
		
		if(!sistemOrigen.isPresent()) {
			throw new InvalidDataException("El sistema origen ingresado es incorrecto.");
		}
		
		List<CAT_SO_FP> catalogos = soFPRepository.findByFkSistemaOrigen(sistemOrigen.get());
		respuesta.setTiposPago(new ArrayList<CAT_TiposPago>());
		
		
		List<CAT_TiposPago> listado = new ArrayList<CAT_TiposPago>();
		for(CAT_SO_FP catalogo :catalogos) {
			listado.add(catalogo.getFkTipoPago());
		}
		
		respuesta.setTiposPago(listado);
		
		return respuesta;
	}

}

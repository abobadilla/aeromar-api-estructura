package aeromar.api.pasarela.services.impls;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aeromar.api.configuration.exceptions.InternalErrorException;
import aeromar.api.configuration.exceptions.InvalidDataException;
import aeromar.api.configuration.exceptions.RecordUpdatedException;
import aeromar.api.pasarela.models.AgrupadorDetalle;
import aeromar.api.pasarela.models.CAT_SistemaOrigen;
import aeromar.api.pasarela.models.CAT_TipoAncillarie;
import aeromar.api.pasarela.models.CAT_TiposPago;
import aeromar.api.pasarela.models.CAT_UnidadDeNegocio;
import aeromar.api.pasarela.models.DetalleHeader;
import aeromar.api.pasarela.models.Etiquetas;
import aeromar.api.pasarela.models.Header;
import aeromar.api.pasarela.models.ValorEtiqueta;
import aeromar.api.pasarela.repositories.AgrupadorDetalleRepository;
import aeromar.api.pasarela.repositories.CAT_SistemaOrigenRepository;
import aeromar.api.pasarela.repositories.CAT_TipoAncillarieRepository;
import aeromar.api.pasarela.repositories.CAT_TiposPagoRepository;
import aeromar.api.pasarela.repositories.CAT_UnidadDeNegocioRepository;
import aeromar.api.pasarela.repositories.DetalleHeaderRepository;
import aeromar.api.pasarela.repositories.EtiquetaRepository;
import aeromar.api.pasarela.repositories.HeaderRepository;
import aeromar.api.pasarela.repositories.ValorEtiquetaRepository;
import aeromar.api.pasarela.services.AdministradorService;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 24 feb. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Service
public class AdministradorServiceImpls implements AdministradorService{


	@Autowired
	private HeaderRepository headerRepository;
	
	@Autowired
	private DetalleHeaderRepository detalleHeaderRepository;
	
	@Autowired
	private CAT_SistemaOrigenRepository sistemaOrigenRepository;
	
	@Autowired
	private CAT_UnidadDeNegocioRepository unidadNegocioRepository;
	
	@Autowired
	private AgrupadorDetalleRepository agrupadorDetalleRepository;
	
	@Autowired
	private EtiquetaRepository etiquetaRepository;
	
	@Autowired
	private ValorEtiquetaRepository valorEtiquetaRepository;
	
	@Autowired
	private CAT_TiposPagoRepository tiposPagoRepository;
	
	@Autowired
	private CAT_TipoAncillarieRepository tipoAncillarieRepository;
	
	
	/**
	 *
	 * Descripción: Genera una nueva solicitud de Vuelo
	 *
	 * @param solicitud
	 * @return
	 */
	@Override
	@Transactional
	public Header generarSolicitudVuelo(Header solicitud) { 
		
		int idUsuario = 1;
	
		// Validar Datos de cliente
		if(solicitud.getNombreContacto() == null || solicitud.getNombreContacto().isEmpty()) {
		    throw new InvalidDataException("El nombre del contacto no puede estar vacío.");
		}
		
		if(solicitud.getApellidoPContacto() == null || solicitud.getApellidoPContacto().isEmpty()) {
		    throw new InvalidDataException("El campo de apellido paterno no puede estar vacío.");
		}
		
		if(solicitud.getApellidoMContacto() == null || solicitud.getApellidoMContacto().isEmpty()) {
		    throw new InvalidDataException("El campo de apellido paterno no puede estar vacío.");
		}
		
		// Validar tamaño minimo del nombre del usuario
		if(solicitud.getNombreContacto().length() < 2) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el nombre de usuario es invalido.");
		}
		
		if(solicitud.getNombreContacto().length() > 255) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el nombre de usuario es invalido, debe de tener un máximo de 255 caracteres.");
		}
		
		
		// Validar tamaño minimo del apellido paterno del cliente
		if(solicitud.getApellidoPContacto().length() < 2) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el apellido paterno del cliente es invalido.");
		}
		
		if(solicitud.getApellidoPContacto().length() > 255) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el apellido paterno del cliente es invalido, debe de tener un máximo de 255 caracteres.");
		}
		
		// Validar tamaño minimo del apellido materno del cliente
		if(solicitud.getApellidoMContacto().length() < 2) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el apellido materno del cliente es invalido.");
		}
		
		if(solicitud.getApellidoMContacto().length() > 255) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el apellido materno del cliente es invalido, debe de tener un máximo de 255 caracteres.");
		}
		
		// Validar tamaño minimo del correo del cliente
		if(solicitud.getCorreoContacto().length() < 2) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el correo del cliente es invalido.");
		}
		
		if(solicitud.getCorreoContacto().length() > 255) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el apellido materno del cliente es invalido, debe de tener un máximo de 255 caracteres.");
		}
		
		// Validar correo
		if(!Pattern.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$",solicitud.getCorreoContacto())) {
			throw new InvalidDataException("El correo electrónico ingresado contiene un formato incorrecto.");
		}
		
		// Validar Moneda
		if(!(solicitud.getClaveMoneda().equals("MXN") || solicitud.getClaveMoneda().equals("USD"))  ) {
			throw new InvalidDataException("Debe de ingresar un tipo de moneda correcta");
		}
		
		
		// Validar clavereferencia (PNR)
		if(solicitud.getClaveReferencia().length() < 2) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para la clave de la referencia es invalido.");
		}
		
		if(solicitud.getClaveReferencia().length() > 70) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para la clave de la referencia del cliente es invalido, debe de tener un máximo de 70 caracteres.");
		}
		
		// Validar telefono celular
		if(!(solicitud.getTelefono()== null || solicitud.getTelefono().isEmpty())) {
			if(!Pattern.matches("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$",solicitud.getTelefono()) || solicitud.getTelefono().length()>21) {
				throw new InvalidDataException("El numero celular ingresado no cuenta con un formato correcto.");
			}
		}
		
		// Validar Office ID
		if(solicitud.getOfficeID().length() < 2) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el Office ID es invalido.");
		}
		
		if(solicitud.getOfficeID().length() > 70) {
			throw new InvalidDataException("El tamaño de caracteres ingresados para el Office Id del cliente es invalido, debe de tener un máximo de 100 caracteres.");
		}
		
		
		// Validar Sistema Origen
		if(solicitud.getFkIdSistemaOrigen() == null || solicitud.getFkIdSistemaOrigen().getClave() == null) {
		    throw new InvalidDataException("El campo de clave sistema origen no puede estar vacío.");
		}
		
		// Validar sistema Origen (Existencia)
		Optional<CAT_SistemaOrigen> sistemaOrigen = sistemaOrigenRepository.findByClaveAndEstado(solicitud.getFkIdSistemaOrigen().getClave(), true);
		if(sistemaOrigen.isPresent()) {
			solicitud.setFkIdSistemaOrigen(sistemaOrigen.get());
		}else {
			throw new InvalidDataException("El sistema origen que ingreso no existe.");
		}
		
		// Id usuario (Verificar usuario con clases)
		solicitud.setUsuarioIdAlta(idUsuario);
		
		
		// Asignar montos (Monto a cobrar, Pendiente de cobro y Monto Cobrado)
		solicitud.setMontoCobrar(new BigDecimal("0")); // Asignar valor dependiendo de los boletos y impuestos
		solicitud.setMontoPendienteCobrar(new BigDecimal("0"));
		solicitud.setMontoCobrado(new BigDecimal("0"));
		
		// Asignar Estatus de cobro y Estatus Reembolso
		solicitud.setEstatusCobro("");
		solicitud.setEstatusReembolso("");
		
		
		// Asignar (Boletos, charters, ancillaries y cargas)
		if(solicitud.getDetallesHeader() == null || solicitud.getDetallesHeader().isEmpty()) {
		    throw new InvalidDataException("El header no tiene asignado ninguna unidad de negocio.");
		}
		// Se genera variable para respaldar los detalles del header
		List<DetalleHeader> detallesHeader = solicitud.getDetallesHeader();
		solicitud.setDetallesHeader(new ArrayList<DetalleHeader>());
		
		// Se genera variable para respaldar los detalles del pago
		List<AgrupadorDetalle> pagosDetalles = solicitud.getPagos();
		solicitud.setPagos(new ArrayList<AgrupadorDetalle>());
		
		
		// Crear Header
		try {
			solicitud = headerRepository.save(solicitud);
			solicitud.setFolioRegistro(this.getFolio(solicitud.getIdHeader()));
		} catch (Exception e) {
			throw new InternalErrorException("Verifique los parámetros de entrada en el header.");
		}
		
		// Se asigna en 0 el valor del header con el fin de sumar los boletos
		BigDecimal totalHeader = BigDecimal.ZERO;
		
		// Crear detalles del header
		for (DetalleHeader elementosU : detallesHeader) {
			Optional<CAT_UnidadDeNegocio> unidad = unidadNegocioRepository.findByClaveAndEstado(elementosU.getFkIdUnidadNegocio().getClave(), true);
			if(unidad.isPresent()) {
				elementosU.setFkIdUnidadNegocio(unidad.get());
				
				// Se asigna estatus
				elementosU.setEstatus(true);
				
				// Se asigna el header al detalle
				elementosU.setFkIdHeader(solicitud);
				
				// Se asigna el estatus de reembolso en 0 debido a que aun no existen reembolsos
				elementosU.setEstatusReembolso(false);
				
				// Se asignan datos de identificacion
				elementosU.setUsuarioIdAlta(idUsuario);
				// Salvar el detalle del header (Elemento)
				elementosU = detalleHeaderRepository.save(elementosU);
			
				// Validar existencia de cupones
				if(elementosU.getCupones()== null || elementosU.getCupones().isEmpty()) {
					throw new InvalidDataException("Debe de ingresar cupones de ingreso y de detalle.");
				}

				
				// Variable usada para sumar los importes de los cupones + los impuetos base
				BigDecimal totalBoleto = BigDecimal.ZERO;
				
				// Variable usada para sumar los importes de los cupones en base al impuesto de cada cupon
				BigDecimal totalBoletoImpuestos = BigDecimal.ZERO;
				
				// Se busca cuantos cupones de detalle existen
				List<AgrupadorDetalle> detalleNumero= elementosU.getCupones().stream().filter(cuponB -> (cuponB.getTipo().equals("Detalle"))).collect(Collectors.toList());
				
				// Se busca cuantos cupones de ingreos existen (Sin incluir cupon de impuestos)
				List<AgrupadorDetalle> ingresosNumero= elementosU.getCupones().stream().filter(cuponB -> (cuponB.getTipo().equals("Ingreso"))).collect(Collectors.toList());
				
				// Se busca cuantos cupones de ingresos que contengan impuestos 
				List<AgrupadorDetalle> ingresosConImpNumero = elementosU.getCupones().stream().filter(cuponB -> (cuponB.getTipo().equals("Ingreso Impuestos") )).collect(Collectors.toList());
				
				// debe de existir la misma cantidad de cupones de ingreso como de 
				if(detalleNumero.size() != ingresosNumero.size()) {
					throw new InvalidDataException("Cada cupón de detalle debe de tener un cupón de ingreso y viceversa.");
				}
				
				// Solo debe de existir un cupon de ingresos con los impuetos del boleto
				if(ingresosConImpNumero.size() != 1){
					throw new InvalidDataException("Solo puede existir un cupón de ingresos con los impuestos generales.");
				}
				
				
				
				for (AgrupadorDetalle cupon : elementosU.getCupones()) {
					
					// Validar si existe un cupon de ingreso y detalle
					if(!(cupon.getTipo().equals("Ingreso") || cupon.getTipo().equals("Detalle")  || cupon.getTipo().equals("Ingreso Impuestos")  )) {
						throw new InvalidDataException("El tipo de cupon es invalido.");
					}
					
					// Asignar descripcion al cupon
					if(cupon.getTipo().equals("Ingreso Impuestos") ) {
						cupon.setNumero(0);
						cupon.setDescripcion("Impuestos del boleto: "+ elementosU.getReferencia());
					}else {
						cupon.setDescripcion("Cupon "+cupon.getNumero()+": "+ elementosU.getReferencia());
					}
					
					final String tipo = cupon.getTipo();
					final Integer numeroCupon = cupon.getNumero();
					
					if(!cupon.getTipo().equals("Ingreso Impuestos")) {
						List<AgrupadorDetalle> elementosRepetidos = elementosU.getCupones().stream().filter( varBusq -> varBusq.getTipo().equals(tipo) && varBusq.getNumero().equals(numeroCupon)).collect(Collectors.toList());
						
						if(elementosRepetidos.size() > 1) {
							throw new InvalidDataException("Solo puede existir un cupón de '"+tipo+"' con el numero: "+numeroCupon );
						}
					}
					
					// Se verifica que exista un cupon de ingreso por cada cupon de detalle
					if(cupon.getTipo().equals("Detalle")) {
						elementosU.getCupones().stream().filter(cuponB -> (cuponB.getTipo().equals("Ingreso") &&  cuponB.getNumero().equals(numeroCupon))).findAny().orElseThrow( () -> new InvalidDataException("Para cada cupon de detalle debe de existir un cupon de ingreso."));
					}
					
					// Se asigna el detalle del header en el cupon
					cupon.setFkIdDetalleHeader(elementosU);
					
					// Se asigna el estatus de reembolso 
					cupon.setEstatusReembolso(0);
					
					// Se asigna el estatus
					cupon.setEstatus(true);
					
					// Se setean valores vacios
					cupon.setFkIdTipoPago(null);
					cupon.setFkIdHeader(null);
					
					// Se asignan datos de identificacion
					cupon.setUsuarioIdAlta(idUsuario);
					
					// Se guardan los valores de las etiquetas en una variable temporal con el fin de iterarlas mas adelante 
					// Esto se hace con el fin de no guardar las etiquetas incompletas 
					List<ValorEtiqueta> valoresEti =cupon.getValoresEtiqueta();
					cupon.setValoresEtiqueta(null);
					
					cupon = agrupadorDetalleRepository.save(cupon);
			
					
					// Se asigna en 0 el valor del cupon con el fin de sumar impuestos y tarifas base
					BigDecimal totalCupon = new BigDecimal(0);
					
					
					
					// Verificar valores de Etiquetas 
					if(valoresEti != null && !valoresEti.isEmpty()) {
						for(ValorEtiqueta elementoEti: valoresEti) {
			
							// Verificar clave (Se busca elemento en la base)
							
							String etiquetaClave = elementoEti.getFkIdEtiqueta().getClaveEtiqueta();
							Etiquetas etiqueta=this.etiquetaRepository.findByClaveEtiquetaAndTipoEtiquetaAndEstatus(etiquetaClave, cupon.getTipo(), true).orElseThrow( () -> new InvalidDataException("La etiqueta '"+etiquetaClave+ "' no existe.") );
							

							// Asignar Etiqueta
							elementoEti.setFkIdEtiqueta(etiqueta);
							
							// Asignar agrupador
							elementoEti.setFkIdAgrupadorDetalle(cupon);
							
							// Asegurar valores numericos
							if(cupon.getTipo().equals("Ingreso") || cupon.getTipo().equals("Ingreso Impuestos")) {	
								try {
									totalCupon = totalCupon.add( new BigDecimal(elementoEti.getValor()));
								} catch (NumberFormatException e) {
									throw new InvalidDataException("Ingreso invalido, el siguiente elemento debe de ser numerico: "+elementoEti.getValor() );
								}
							}
							
							// Verificar que la clave ingresada del ancillarie sea correcta
							if(etiquetaClave.equals("DET0003") ) {
								CAT_TipoAncillarie tipoAncilarie = tipoAncillarieRepository.findByClaveAndEstado(elementoEti.getValor(), true).orElseThrow(()->new InvalidDataException("La clave '"+elementoEti.getValor()+"' no corresponde a ningun tipo de ancillarie"));
								tipoAncilarie.getEtiqueta();
								// Se le asigna al boleto el nombre del Ancillarie
								elementosU.setDescripcion("Ancillarie: "+ tipoAncilarie.getEtiqueta());
							}
							
							// Asignar valores generales
							elementoEti.setEstatus(true);
							elementoEti.setUsuarioIdAlta(idUsuario);
							valorEtiquetaRepository.save(elementoEti);
						
						}
						
					}else {
						throw new InvalidDataException("Debe de ingresar etiquetas en los cupones.");
					}
					
					if(cupon.getTipo().equals("Detalle")) {
						cupon.setMontoCupon(null);
					}else {
						cupon.setMontoCupon(totalCupon);	
					}
					totalBoleto = totalBoleto.add(totalCupon);
					totalBoletoImpuestos= totalBoletoImpuestos.add(cupon.getMontoCuponImp()== null ? BigDecimal.ZERO: cupon.getMontoCuponImp());
				}
				
				// Se valida que las etiquetas tengan los mismos montos (con impuestos) ingresados en el cupon
				if(!totalBoletoImpuestos.equals(totalBoleto)) {
					throw new InvalidDataException("Los montos ingresados en las etiquetas no concuerdan con los montos (con impuetos) agregados en el cupon.");
				}
				
				
				// Asignar total de los cupones en el boleto
				elementosU.setMontoBoleto(totalBoleto);

				
				// Sumar unidad de negocio al total del header
				totalHeader = totalHeader.add(totalBoleto);
			
			}else {
				throw new InvalidDataException("La unidad de negocio ingresada no existe: "+elementosU.getFkIdUnidadNegocio().getIdUnidadNegocio()+".");
			}
        }
		
		// Se asigna el total a cobrar del header, en base a las unidades de negicio solicitadas
		solicitud.setMontoCobrar(totalHeader);

	
		// Se crea variable para identificar la numeracion del pago
		Integer numeroPago = 0;
		
		
		// Se crea variable para identificar el total de los pagos
		BigDecimal totalPagos = BigDecimal.ZERO;
		
		// Existe pago referenciado
		Boolean existePagoReferenciado = false;
		
		// Existe pago normal
		Boolean existePagoNormal = false;
		
		// Asignar pagos
		for (AgrupadorDetalle pago : pagosDetalles) {
			pago.setTipo("Pago");
			
			// Asignar header
			pago.setFkIdHeader(solicitud);
			
			// Se asignan datos de identificacion
			pago.setUsuarioIdAlta(idUsuario);
			
			// Asignar estatus
			pago.setEstatus(true);
			
			// Asignar numero de pago
			numeroPago++;
			pago.setNumero(numeroPago);
			
			// Verificar tipo de pago
			CAT_TiposPago tipoPago= tiposPagoRepository.findByClaveAndEstado(pago.getFkIdTipoPago().getClave(), true).orElseThrow( () ->  new InvalidDataException("El tipo de pago ingresado no existe."));
		
			if(tipoPago.getTipoTarjeta() != null && tipoPago.getTipoTarjeta().size() > 0) {
				if(pago.getFkIdTipoPago().getTipoTarjeta() == null || pago.getFkIdTipoPago().getTipoTarjeta().size()== 0) {
					throw new InvalidDataException("Debe de ingresar un tipo de tarjeta.");
				}else {
					// Buscar si la tarjeta solicitante existe
					final String tarjetaSolicitante = pago.getFkIdTipoPago().getTipoTarjeta().get(0).getClave();
					tipoPago.getTipoTarjeta().stream().filter(tarjeta -> tarjeta.getClave().equals(tarjetaSolicitante) ).findAny().orElseThrow(() ->new InvalidDataException("El tipo de tarjeta '"+tarjetaSolicitante+"' no existe."));
					
				}
			}
			
			
			// Se asigna el tipo de pago al agrupador
			pago.setDescripcion(tipoPago.getEtiqueta());
			pago.setFkIdTipoPago(tipoPago);
	
			
			// Guardar en variable auxiliar l;as etiquetas
			List<ValorEtiqueta> valoresEti =pago.getValoresEtiqueta();
			pago.setValoresEtiqueta(null);
			
			// Guardar pago
			pago = agrupadorDetalleRepository.save(pago);
			
			
			// -------------> Etiquetas mandatorias
			// Verificar que exista una etiqueta referencia de pago
			List<ValorEtiqueta> referenciasPago =valoresEti.stream().filter(etiquetaValor -> etiquetaValor.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0001")).collect(Collectors.toList());
			if(referenciasPago.size() != 1) {
				throw new InvalidDataException("Debe de ingresar al menos una etiqueta 'PAY0001'");
			}
			
			
			// Verificar que exista una etiqueta monto
			List<ValorEtiqueta> montos =valoresEti.stream().filter(etiquetaValor -> etiquetaValor.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0002")).collect(Collectors.toList());
			if(montos.size() != 1) {
				throw new InvalidDataException("Debe de ingresar al menos una etiqueta 'PAY0002'");
			}
			
			// Verificar que exista una etiqueta de fecha y hora de operacion
			List<ValorEtiqueta> fechasOperacion =valoresEti.stream().filter(etiquetaValor -> etiquetaValor.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0005")).collect(Collectors.toList());
			if(fechasOperacion.size() != 1) {
				throw new InvalidDataException("Debe de ingresar al menos una etiqueta 'PAY0005'");
			}
			
			// Verificar que exista un estatus de operacion
			List<ValorEtiqueta> estatusOperacion =valoresEti.stream().filter(etiquetaValor -> etiquetaValor.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0007")).collect(Collectors.toList());
			if(estatusOperacion.size() > 0) {
				throw new InvalidDataException("La etiqueta 'PAY0007' no esta permitida.");
			}
			// <------------- Etiqueta que no debe de existir
			
			
			// Verificar que exista un estatus de reembolso
			List<ValorEtiqueta> estatusOperacionR =valoresEti.stream().filter(etiquetaValor -> etiquetaValor.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0010")).collect(Collectors.toList());
			if(estatusOperacionR.size() > 0) {
				throw new InvalidDataException("La etiqueta 'PAY0010' no esta permitida.");
			}
			// <------------- Etiqueta que no debe de existir
			

			
			//se busca la etiqueta (Afiliación en donde ocurre el cobro)
			Etiquetas etiquetaEstatusPago=this.etiquetaRepository.findByClaveEtiquetaAndTipoEtiquetaAndEstatus("PAY0007", "Pago", true).orElseThrow( () -> new InvalidDataException("Ocurrio un error al generar el estatus de pago.") );
			
			// Verificar (Tiempo)
			if(etiquetaEstatusPago.getFecFin() != null) {
				if(!(etiquetaEstatusPago.getFecInicio().compareTo(new Date())>=0 && etiquetaEstatusPago.getFecFin().compareTo(new Date())<=0)) {
					throw new InvalidDataException("La etiqueta '"+etiquetaEstatusPago.getDescripcion()+"' expiro.");
				}
			}
			
			// Se crea valor etiqueta estatus operacion
			ValorEtiqueta etiquetaValorEstatus= new ValorEtiqueta();
			etiquetaValorEstatus.setFkIdEtiqueta(etiquetaEstatusPago);
			etiquetaValorEstatus.setFkIdAgrupadorDetalle(pago);
			etiquetaValorEstatus.setEstatus(true);
			etiquetaValorEstatus.setUsuarioIdAlta(idUsuario);	
			
			
			
			//se busca la etiqueta (Estatus de Reembolso detalle del pago)
			Etiquetas etiquetaEstatusR=this.etiquetaRepository.findByClaveEtiquetaAndTipoEtiquetaAndEstatus("PAY0010", "Pago", true).orElseThrow( () -> new InvalidDataException("Ocurrio un error al generar el estatus de reembolso.") );
			
			// Verificar (Tiempo)
			if(etiquetaEstatusR.getFecFin() != null) {
				if(!(etiquetaEstatusR.getFecInicio().compareTo(new Date())>=0 && etiquetaEstatusR.getFecFin().compareTo(new Date())<=0)) {
					throw new InvalidDataException("La etiqueta '"+etiquetaEstatusR.getDescripcion()+"' expiro.");
				}
			}
			
			// Se crea valor etiqueta estatus operacion
			ValorEtiqueta etiquetaValorEstatusR= new ValorEtiqueta();
			etiquetaValorEstatusR.setFkIdEtiqueta(etiquetaEstatusR);
			etiquetaValorEstatusR.setFkIdAgrupadorDetalle(pago);
			etiquetaValorEstatusR.setEstatus(true);
			etiquetaValorEstatusR.setUsuarioIdAlta(idUsuario);	
			this.valorEtiquetaRepository.save(etiquetaValorEstatusR);
			

			
			// Iterar etiquetas
			for(ValorEtiqueta valEtiqueta : valoresEti) {
				// Verificar etiquetas repetidas
				final String etiquetaLabel = valEtiqueta.getFkIdEtiqueta().getClaveEtiqueta();
				
				List<ValorEtiqueta> etiquetasRepetidas =valoresEti.stream().filter(etiquetaValor -> etiquetaValor.getFkIdEtiqueta().getClaveEtiqueta().equals(etiquetaLabel)).collect(Collectors.toList());
				if(etiquetasRepetidas.size() != 1) {
					throw new InvalidDataException("Solo puede existir una etiqueta de pago: "+ etiquetaLabel);
				}
				
				if(valEtiqueta.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0001") || valEtiqueta.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0002") || valEtiqueta.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0005") || valEtiqueta.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0007")  ) {
					if(valEtiqueta.getValor() == null || valEtiqueta.getValor().length() < 2) {
						throw new InvalidDataException("Debe de ingresar un valor valido para la etiqueta: "+ etiquetaLabel);
					}
				}
				
				
				// Buscar Etiqueta
				Etiquetas etiqueta=this.etiquetaRepository.findByClaveEtiquetaAndTipoEtiquetaAndEstatus(etiquetaLabel, "Pago", true).orElseThrow(() -> new InvalidDataException("La etiqueta '"+etiquetaLabel+"' no existe."));
				
				// Verificar fecha de la etiqueta
				boolean fechaValida =false;
				
				if(etiqueta.getFecInicio().compareTo(new Date())<=0 && etiqueta.getFecFin()== null) {
					fechaValida = true;
				}else {
					if(etiqueta.getFecInicio().compareTo(new Date())<=0 && etiqueta.getFecFin().compareTo(new Date())>=0 ) {
						fechaValida = true;
					}
				}
				
				if(fechaValida) {
					// verificando dato de etiqueta 'Monto'
					if(etiqueta.getClaveEtiqueta().equals("PAY0002")) {
						// Si existe pago referenciado
						if(tipoPago.getClave().equals("PG0005")) {
							// Los pagos referenciados pueden tener un monto de 0 debido a que se pagaran despues
							existePagoReferenciado = true;
						}else {
							existePagoNormal =true;
						}
						// Asegurar valor monetario
						try {
							BigDecimal valorMonto = new BigDecimal(valEtiqueta.getValor());
							if(valorMonto.compareTo(BigDecimal.ZERO) == 0) {
								throw new InvalidDataException("Los pagos no pueden estar en cero.");
							}
							valEtiqueta.setValor(valorMonto.toString());
							if(!existePagoReferenciado) {
								totalPagos = totalPagos.add(valorMonto);
							}
							
							pago.setMontoCupon(totalPagos);
						} catch (NumberFormatException e) {
							throw new InvalidDataException("El monto ingresado en el pago tiene un formato incorrecto: "+valEtiqueta.getValor() );
						}
					}
					
					// Se asigna el valor del estatus del pago
					if(existePagoReferenciado) {
						etiquetaValorEstatus.setValor("Pendiente");
					}else {
						etiquetaValorEstatus.setValor("Aprobado");
					}
					this.valorEtiquetaRepository.save(etiquetaValorEstatus);
					
					// Asignar etiqueta
					valEtiqueta.setFkIdEtiqueta(etiqueta);
					
					// Se asigna agrupador a la etiqueta
					valEtiqueta.setFkIdAgrupadorDetalle(pago);
					
					
					// Se asignan valores de registro
					valEtiqueta.setUsuarioIdAlta(idUsuario);
					valEtiqueta.setEstatus(true);
					
					// Se guarda etiqueta
					valorEtiquetaRepository.save(valEtiqueta);
				}
				
			}
			
		}
		
		
		if(existePagoNormal && existePagoReferenciado) {
			solicitud.setEstatusCobro("Cobro Parcial");
			solicitud.setMontoPendienteCobrar(totalHeader.subtract(totalPagos));
			solicitud.setMontoCobrado(totalPagos);
		}
		
		if(existePagoNormal ==false && existePagoReferenciado) {
			solicitud.setEstatusCobro("Pendiente de Cobro");
			solicitud.setMontoPendienteCobrar(totalHeader);
			solicitud.setMontoCobrado(BigDecimal.ZERO);
		}
		
		
		if(existePagoNormal && existePagoReferenciado == false) {
			if(totalPagos!= totalHeader) {
				throw new InvalidDataException("Los montos ingresados son invalidos." );
			}
			solicitud.setEstatusCobro("Cobrado");
			solicitud.setMontoCobrado(totalPagos);
		}
		
		
		return solicitud;
	}
	
	
	
	/**
	 *
	 * Descripción: Punto de acceso de pagos referenciados
	 *
	 * @param solicitud
	 * @param afiliacion
	 * @return
	 * @see aeromar.api.pasarela.services.AdministradorService#pagoReferenciado(java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional
	public String pagoReferenciado(String solicitud, String afiliacion ) { 
		//se busca la etiqueta (Referencia de pago)
		ValorEtiqueta valorAgrupador = valorEtiquetaRepository.findByFkIdEtiquetaClaveEtiquetaAndValorAndEstatus("PAY0001", solicitud,true).orElseThrow( () ->  new InvalidDataException("La referencia agregada no existe."));
		
		
		List<ValorEtiqueta> valorEstatus = valorAgrupador.getFkIdAgrupadorDetalle().getValoresEtiqueta().stream().filter(valEti -> valEti.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0007") ).collect(Collectors.toList());
		
		if(valorEstatus.size() == 0) {
			throw  new InvalidDataException("Debe de existir una etiqueta de estatus.");
		}
		
		// Se busca el estatus del pago
		
		
		if(!(valorEstatus.get(0).getValor().equals("Pendiente") || valorEstatus.get(0).getValor().equals("Cobro Parcial"))) {
			throw  new InvalidDataException("El estatus del pago debe de ser pendiente o Cobro Parcial.");
		}
		
		//se busca la etiqueta (Afiliación en donde ocurre el cobro)
		Etiquetas etiqueta=this.etiquetaRepository.findByClaveEtiquetaAndTipoEtiquetaAndEstatus("PAY0015", "Pago", true).orElseThrow( () -> new InvalidDataException("La etiqueta 'PAY0015' no existe.") );
		
		
		// Verificar etiqueta (Tiempo)
		if(etiqueta.getFecFin() != null) {
			if(!(etiqueta.getFecInicio().compareTo(new Date())>=0 && etiqueta.getFecFin().compareTo(new Date())<=0)) {
				throw new InvalidDataException("La etiqueta '"+etiqueta.getDescripcion()+"' expiro.");
			}
		}
		
		// Se crea valor etiqueta Afiliación en donde ocurre el cobro
		ValorEtiqueta etiquetaAfiliacion= new ValorEtiqueta();
		etiquetaAfiliacion.setFkIdEtiqueta(etiqueta);
		etiquetaAfiliacion.setFkIdAgrupadorDetalle(valorAgrupador.getFkIdAgrupadorDetalle());
		etiquetaAfiliacion.setValor(afiliacion);
		etiquetaAfiliacion.setEstatus(true);
		etiquetaAfiliacion.setUsuarioIdAlta(null);	
		this.valorEtiquetaRepository.save(etiquetaAfiliacion);
		
		valorEstatus.get(0).setValor("Aprobado");
		
		// Se le asigna un estatus de cobrado al header
		Header base = valorAgrupador.getFkIdAgrupadorDetalle().getFkIdHeader();
		base.setEstatusCobro("Cobrado");
	
		List<ValorEtiqueta> valorPago = valorAgrupador.getFkIdAgrupadorDetalle().getValoresEtiqueta().stream().filter(valEti -> valEti.getFkIdEtiqueta().getClaveEtiqueta().equals("PAY0002") ).collect(Collectors.toList());
		
		if(valorEstatus.size() == 0) {
			throw  new InvalidDataException("Debe de existir una etiqueta de monto.");
		}
		
		valorPago.get(0).getValor();
		
		base.setMontoPendienteCobrar( base.getMontoPendienteCobrar().subtract(new BigDecimal(valorPago.get(0).getValor())) );
		base.setMontoCobrado( base.getMontoCobrado().add(new BigDecimal(valorPago.get(0).getValor())) );
		
		return solicitud;
		
	}
	
	
	/**
	 *
	 * Descripción: Genera un folio en base a un id
	 *
	 * @param id
	 * @return String
	 */
	private String getFolio(Integer id) {
		Integer cambioLetra=100000;
		Integer letraBase= (id/cambioLetra);
		Integer idFolio= (id-cambioLetra*letraBase)==0 ?  id : (id-cambioLetra*letraBase ==cambioLetra ) ? 0 : (id-cambioLetra*letraBase) ;
		String letras[] = new String[] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		idFolio = (idFolio== cambioLetra) ? 0 : idFolio;
		
	
		if(letraBase > 26) {
			//letrabase1 = letraBase/26;
			letraBase = letraBase%26;
		}
		
		return "A"+letras[letraBase]+String.format("%05d", idFolio);
	}

}

package aeromar.api.pasarela.services;


import aeromar.api.pasarela.models.Header;


public interface AdministradorService {
	public Header generarSolicitudVuelo(Header solicitud);
	public String pagoReferenciado(String solicitud, String afiliacion);
}

package aeromar.api.pasarela.services;

import aeromar.api.pasarela.models.ConfiguradorRespuesta;
import aeromar.api.pasarela.models.RequestConfigurador;

public interface ConfiguradorService {
	public ConfiguradorRespuesta obtenerDatos(RequestConfigurador rConf);
}

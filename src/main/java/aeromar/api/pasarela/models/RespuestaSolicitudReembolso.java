package aeromar.api.pasarela.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import aeromar.api.configuration.models.BitacoraRegistro;


@Entity
@Table(name = "PP_RespuestaSolicitudReembolso")
public class RespuestaSolicitudReembolso  extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RespuestaSolicitudReembolso", nullable = false)
	private Integer idRespuestaSolicitudReembolso;
	
	@Column(name = "FECHA")
	@Temporal(TemporalType.DATE)
	private Date fecNac;
	
	@Column(name = "DESCRIPCION", columnDefinition="TEXT")
	private String descripcion;
	
	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ID_SolicitudReembolso", referencedColumnName  = "ID_SolicitudReembolso")
	private SolicitudReembolso fkIdSolicitudReembolso;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idRespuestaSolicitudReembolso
	 * @param fecNac
	 * @param descripcion
	 * @param fkIdSolicitudReembolso
	 */
	public RespuestaSolicitudReembolso(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idRespuestaSolicitudReembolso, Date fecNac, String descripcion,
			SolicitudReembolso fkIdSolicitudReembolso) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idRespuestaSolicitudReembolso = idRespuestaSolicitudReembolso;
		this.fecNac = fecNac;
		this.descripcion = descripcion;
		this.fkIdSolicitudReembolso = fkIdSolicitudReembolso;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public RespuestaSolicitudReembolso() {
		super();
	}


	/**
	 * Método para recuperar el valor de la propiedad idRespuestaSolicitudReembolso
	 *
	 * @return the idRespuestaSolicitudReembolso
	 */
	public Integer getIdRespuestaSolicitudReembolso() {
		return idRespuestaSolicitudReembolso;
	}

	/**
	 * Método para establecer el valor de una propiedad idRespuestaSolicitudReembolso
	 *
	 * @param idRespuestaSolicitudReembolso the idRespuestaSolicitudReembolso to set
	 */
	public void setIdRespuestaSolicitudReembolso(Integer idRespuestaSolicitudReembolso) {
		this.idRespuestaSolicitudReembolso = idRespuestaSolicitudReembolso;
	}

	/**
	 * Método para recuperar el valor de la propiedad fecNac
	 *
	 * @return the fecNac
	 */
	public Date getFecNac() {
		return fecNac;
	}

	/**
	 * Método para establecer el valor de una propiedad fecNac
	 *
	 * @param fecNac the fecNac to set
	 */
	public void setFecNac(Date fecNac) {
		this.fecNac = fecNac;
	}

	/**
	 * Método para recuperar el valor de la propiedad descripcion
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Método para establecer el valor de una propiedad descripcion
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdSolicitudReembolso
	 *
	 * @return the fkIdSolicitudReembolso
	 */
	public SolicitudReembolso getFkIdSolicitudReembolso() {
		return fkIdSolicitudReembolso;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdSolicitudReembolso
	 *
	 * @param fkIdSolicitudReembolso the fkIdSolicitudReembolso to set
	 */
	public void setFkIdSolicitudReembolso(SolicitudReembolso fkIdSolicitudReembolso) {
		this.fkIdSolicitudReembolso = fkIdSolicitudReembolso;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RespuestaSolicitudReembolso [idRespuestaSolicitudReembolso=");
		builder.append(idRespuestaSolicitudReembolso);
		builder.append(", fecNac=");
		builder.append(fecNac);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(", fkIdSolicitudReembolso=");
		builder.append(fkIdSolicitudReembolso);
		builder.append("]");
		return builder.toString();
	}
	
	

}

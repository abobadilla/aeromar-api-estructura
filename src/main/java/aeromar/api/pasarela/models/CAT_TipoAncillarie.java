package aeromar.api.pasarela.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import aeromar.api.configuration.models.BitacoraRegistro;


@Entity
@Table(name = "PP_CAT_TipoAncillarie", indexes = { @Index(name = "IDX_PP_CLAVE", columnList = "CLAVE", unique = true) })
public class CAT_TipoAncillarie extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TipoAncillarie", nullable = false)
	private Integer idTipoAncillarie;
	
	@Column(name = "CLAVE", length = 50)
	private String clave;
	
	@Column(name = "ETIQUETA", length = 70)
	private String etiqueta;
	
	@Column(name = "ESTADO")
	private Boolean estado;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param idTipoAncillarie
	 * @param clave
	 * @param etiqueta
	 * @param estado
	 */
	public CAT_TipoAncillarie(Integer idTipoAncillarie, String clave, String etiqueta, Boolean estado) {
		super();
		this.idTipoAncillarie = idTipoAncillarie;
		this.clave = clave;
		this.etiqueta = etiqueta;
		this.estado = estado;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public CAT_TipoAncillarie() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad idTipoAncillarie
	 *
	 * @return the idTipoAncillarie
	 */
	public Integer getIdTipoAncillarie() {
		return idTipoAncillarie;
	}

	/**
	 * Método para establecer el valor de una propiedad idTipoAncillarie
	 *
	 * @param idTipoAncillarie the idTipoAncillarie to set
	 */
	public void setIdTipoAncillarie(Integer idTipoAncillarie) {
		this.idTipoAncillarie = idTipoAncillarie;
	}

	/**
	 * Método para recuperar el valor de la propiedad clave
	 *
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Método para establecer el valor de una propiedad clave
	 *
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * Método para recuperar el valor de la propiedad etiqueta
	 *
	 * @return the etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad etiqueta
	 *
	 * @param etiqueta the etiqueta to set
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad estado
	 *
	 * @return the estado
	 */
	public Boolean getEstado() {
		return estado;
	}

	/**
	 * Método para establecer el valor de una propiedad estado
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CAT_TipoAncillarie [idTipoAncillarie=");
		builder.append(idTipoAncillarie);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", etiqueta=");
		builder.append(etiqueta);
		builder.append(", estado=");
		builder.append(estado);
		builder.append("]");
		return builder.toString();
	}
	
}



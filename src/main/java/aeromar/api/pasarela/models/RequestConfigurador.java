package aeromar.api.pasarela.models;

public class RequestConfigurador {
	public String claveSO;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param claveSO
	 */
	public RequestConfigurador(String claveSO) {
		super();
		this.claveSO = claveSO;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public RequestConfigurador() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad claveUnidad
	 *
	 * @return the claveUnidad
	 */
	public String getClaveSO() {
		return claveSO;
	}

	/**
	 * Método para establecer el valor de una propiedad claveUnidad
	 *
	 * @param claveUnidad the claveUnidad to set
	 */
	public void setClaveUnidad(String claveSO) {
		this.claveSO = claveSO;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequestConfigurador [claveSO=");
		builder.append(claveSO);
		builder.append("]");
		return builder.toString();
	}
	
}

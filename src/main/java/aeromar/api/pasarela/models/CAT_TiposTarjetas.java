package aeromar.api.pasarela.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import aeromar.api.configuration.models.BitacoraRegistro;

@Entity
@Table(name = "PP_CAT_TiposTarjetas", indexes = { @Index(name = "IDX_PP_CLAVE", columnList = "CLAVE", unique = true) })
public class CAT_TiposTarjetas extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TipoPago", nullable = false)
	private Integer idTipoPago;
	
	@Column(name = "CLAVE", length = 50)
	private String clave;
	
	@Column(name = "ETIQUETA", length = 70)
	private String etiqueta;
	
	@Column(name = "CLAVE_AMADEUS", length = 80)
	private String claveAmadeus;

	@Column(name = "ESTADO")
	private Boolean estado;

	@JsonBackReference
	@ManyToOne
    @JoinColumn(name = "FK_ID_TipoPago")
    private CAT_TiposPago tipoPago;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idTipoPago
	 * @param clave
	 * @param etiqueta
	 * @param claveAmadeus
	 * @param estado
	 * @param tipoPago
	 */
	public CAT_TiposTarjetas(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idTipoPago, String clave, String etiqueta, String claveAmadeus, Boolean estado,
			CAT_TiposPago tipoPago) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idTipoPago = idTipoPago;
		this.clave = clave;
		this.etiqueta = etiqueta;
		this.claveAmadeus = claveAmadeus;
		this.estado = estado;
		this.tipoPago = tipoPago;
	}

	

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public CAT_TiposTarjetas() {
		super();
	}



	/**
	 * Método para recuperar el valor de la propiedad idTipoPago
	 *
	 * @return the idTipoPago
	 */
	public Integer getIdTipoPago() {
		return idTipoPago;
	}

	/**
	 * Método para establecer el valor de una propiedad idTipoPago
	 *
	 * @param idTipoPago the idTipoPago to set
	 */
	public void setIdTipoPago(Integer idTipoPago) {
		this.idTipoPago = idTipoPago;
	}

	/**
	 * Método para recuperar el valor de la propiedad clave
	 *
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Método para establecer el valor de una propiedad clave
	 *
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * Método para recuperar el valor de la propiedad etiqueta
	 *
	 * @return the etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad etiqueta
	 *
	 * @param etiqueta the etiqueta to set
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad claveAmadeus
	 *
	 * @return the claveAmadeus
	 */
	public String getClaveAmadeus() {
		return claveAmadeus;
	}

	/**
	 * Método para establecer el valor de una propiedad claveAmadeus
	 *
	 * @param claveAmadeus the claveAmadeus to set
	 */
	public void setClaveAmadeus(String claveAmadeus) {
		this.claveAmadeus = claveAmadeus;
	}

	/**
	 * Método para recuperar el valor de la propiedad estado
	 *
	 * @return the estado
	 */
	public Boolean getEstado() {
		return estado;
	}

	/**
	 * Método para establecer el valor de una propiedad estado
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	/**
	 * Método para recuperar el valor de la propiedad tipoPago
	 *
	 * @return the tipoPago
	 */
	public CAT_TiposPago getTipoPago() {
		return tipoPago;
	}

	/**
	 * Método para establecer el valor de una propiedad tipoPago
	 *
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(CAT_TiposPago tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CAT_TiposTarjetas [idTipoPago=");
		builder.append(idTipoPago);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", etiqueta=");
		builder.append(etiqueta);
		builder.append(", claveAmadeus=");
		builder.append(claveAmadeus);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", tipoPago=");
		builder.append(tipoPago);
		builder.append("]");
		return builder.toString();
	}

	
    
    
}

package aeromar.api.pasarela.models;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import aeromar.api.configuration.models.BitacoraRegistro;



/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 2 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Entity
@Table(name = "PP_AgrupadorDetalle")
public class AgrupadorDetalle extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_AgrupadorDetalle", nullable = false)
	private Integer idAgrupadorDetalle;
	 
	
	@Column(name = "TIPO", length = 30)
	private String tipo;
	
	@Column(name = "NUMERO")
	private Integer numero;
	
	@Column(name = "DESCRIPCION", length = 70)
	private String descripcion;
	
	@Column(name = "ESTATUS")
	private Boolean estatus;
	
	// Monto Cupon
	@Column(name = "MONTO_CUPON")
	private BigDecimal montoCupon;
	
	// Monto Cupon con impuestos
	@Column(name = "MONTO_CUPON_IMP")
	private BigDecimal montoCuponImp;
	

	/*
	 * 0 Sin reembolso
	 * 1 Parcialmente reembolsado
	 * 2 Reembolsado
	 */
	@Column(name = "ESTATUS_REEMBOLSO")
	private Integer estatusReembolso;
	
	
	
	// Unidad de negocio (Boletos)
	@JsonBackReference
	@ManyToOne
    @JoinColumn(name = "FK_ID_DetalleHeader")
    private DetalleHeader fkIdDetalleHeader;
	
	// Pagos
	@JsonBackReference(value="user-payment")
	@ManyToOne
    @JoinColumn(name = "FK_ID_Header")
    private Header fkIdHeader;
	
	@JsonManagedReference
	@OneToMany(mappedBy="fkIdAgrupadorDetalle")
    private List<ValorEtiqueta> valoresEtiqueta;
	
	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ID_TipoPago", referencedColumnName  = "ID_TipoPago")
	private CAT_TiposPago fkIdTipoPago;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param idAgrupadorDetalle
	 * @param tipo
	 * @param numero
	 * @param descripcion
	 * @param estatus
	 * @param montoCupon
	 * @param montoCuponImp
	 * @param estatusReembolso
	 * @param fkIdDetalleHeader
	 * @param fkIdHeader
	 * @param valoresEtiqueta
	 * @param fkIdTipoPago
	 */
	public AgrupadorDetalle(Integer idAgrupadorDetalle, String tipo, Integer numero, String descripcion,
			Boolean estatus, BigDecimal montoCupon, BigDecimal montoCuponImp, Integer estatusReembolso,
			DetalleHeader fkIdDetalleHeader, Header fkIdHeader, List<ValorEtiqueta> valoresEtiqueta,
			CAT_TiposPago fkIdTipoPago) {
		super();
		this.idAgrupadorDetalle = idAgrupadorDetalle;
		this.tipo = tipo;
		this.numero = numero;
		this.descripcion = descripcion;
		this.estatus = estatus;
		this.montoCupon = montoCupon;
		this.montoCuponImp = montoCuponImp;
		this.estatusReembolso = estatusReembolso;
		this.fkIdDetalleHeader = fkIdDetalleHeader;
		this.fkIdHeader = fkIdHeader;
		this.valoresEtiqueta = valoresEtiqueta;
		this.fkIdTipoPago = fkIdTipoPago;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public AgrupadorDetalle() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad idAgrupadorDetalle
	 *
	 * @return the idAgrupadorDetalle
	 */
	public Integer getIdAgrupadorDetalle() {
		return idAgrupadorDetalle;
	}

	/**
	 * Método para establecer el valor de una propiedad idAgrupadorDetalle
	 *
	 * @param idAgrupadorDetalle the idAgrupadorDetalle to set
	 */
	public void setIdAgrupadorDetalle(Integer idAgrupadorDetalle) {
		this.idAgrupadorDetalle = idAgrupadorDetalle;
	}

	/**
	 * Método para recuperar el valor de la propiedad tipo
	 *
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Método para establecer el valor de una propiedad tipo
	 *
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Método para recuperar el valor de la propiedad numero
	 *
	 * @return the numero
	 */
	public Integer getNumero() {
		return numero;
	}

	/**
	 * Método para establecer el valor de una propiedad numero
	 *
	 * @param numero the numero to set
	 */
	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	/**
	 * Método para recuperar el valor de la propiedad descripcion
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Método para establecer el valor de una propiedad descripcion
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatus
	 *
	 * @return the estatus
	 */
	public Boolean getEstatus() {
		return estatus;
	}

	/**
	 * Método para establecer el valor de una propiedad estatus
	 *
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

	/**
	 * Método para recuperar el valor de la propiedad montoCupon
	 *
	 * @return the montoCupon
	 */
	public BigDecimal getMontoCupon() {
		return montoCupon;
	}

	/**
	 * Método para establecer el valor de una propiedad montoCupon
	 *
	 * @param montoCupon the montoCupon to set
	 */
	public void setMontoCupon(BigDecimal montoCupon) {
		this.montoCupon = montoCupon;
	}

	/**
	 * Método para recuperar el valor de la propiedad montoCuponImp
	 *
	 * @return the montoCuponImp
	 */
	public BigDecimal getMontoCuponImp() {
		return montoCuponImp;
	}

	/**
	 * Método para establecer el valor de una propiedad montoCuponImp
	 *
	 * @param montoCuponImp the montoCuponImp to set
	 */
	public void setMontoCuponImp(BigDecimal montoCuponImp) {
		this.montoCuponImp = montoCuponImp;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatusReembolso
	 *
	 * @return the estatusReembolso
	 */
	public Integer getEstatusReembolso() {
		return estatusReembolso;
	}

	/**
	 * Método para establecer el valor de una propiedad estatusReembolso
	 *
	 * @param estatusReembolso the estatusReembolso to set
	 */
	public void setEstatusReembolso(Integer estatusReembolso) {
		this.estatusReembolso = estatusReembolso;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdDetalleHeader
	 *
	 * @return the fkIdDetalleHeader
	 */
	public DetalleHeader getFkIdDetalleHeader() {
		return fkIdDetalleHeader;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdDetalleHeader
	 *
	 * @param fkIdDetalleHeader the fkIdDetalleHeader to set
	 */
	public void setFkIdDetalleHeader(DetalleHeader fkIdDetalleHeader) {
		this.fkIdDetalleHeader = fkIdDetalleHeader;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdHeader
	 *
	 * @return the fkIdHeader
	 */
	public Header getFkIdHeader() {
		return fkIdHeader;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdHeader
	 *
	 * @param fkIdHeader the fkIdHeader to set
	 */
	public void setFkIdHeader(Header fkIdHeader) {
		this.fkIdHeader = fkIdHeader;
	}

	/**
	 * Método para recuperar el valor de la propiedad valoresEtiqueta
	 *
	 * @return the valoresEtiqueta
	 */
	public List<ValorEtiqueta> getValoresEtiqueta() {
		return valoresEtiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad valoresEtiqueta
	 *
	 * @param valoresEtiqueta the valoresEtiqueta to set
	 */
	public void setValoresEtiqueta(List<ValorEtiqueta> valoresEtiqueta) {
		this.valoresEtiqueta = valoresEtiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdTipoPago
	 *
	 * @return the fkIdTipoPago
	 */
	public CAT_TiposPago getFkIdTipoPago() {
		return fkIdTipoPago;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdTipoPago
	 *
	 * @param fkIdTipoPago the fkIdTipoPago to set
	 */
	public void setFkIdTipoPago(CAT_TiposPago fkIdTipoPago) {
		this.fkIdTipoPago = fkIdTipoPago;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AgrupadorDetalle [idAgrupadorDetalle=");
		builder.append(idAgrupadorDetalle);
		builder.append(", tipo=");
		builder.append(tipo);
		builder.append(", numero=");
		builder.append(numero);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(", estatus=");
		builder.append(estatus);
		builder.append(", montoCupon=");
		builder.append(montoCupon);
		builder.append(", montoCuponImp=");
		builder.append(montoCuponImp);
		builder.append(", estatusReembolso=");
		builder.append(estatusReembolso);
		builder.append(", fkIdDetalleHeader=");
		builder.append(fkIdDetalleHeader);
		builder.append(", fkIdHeader=");
		builder.append(fkIdHeader);
		builder.append(", valoresEtiqueta=");
		builder.append(valoresEtiqueta);
		builder.append(", fkIdTipoPago=");
		builder.append(fkIdTipoPago);
		builder.append("]");
		return builder.toString();
	}
	
	
	


}

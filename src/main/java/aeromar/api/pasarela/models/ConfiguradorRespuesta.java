package aeromar.api.pasarela.models;

import java.util.List;

public class ConfiguradorRespuesta {
	public List<CAT_TiposPago> tiposPago;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param tiposPago
	 */
	public ConfiguradorRespuesta(List<CAT_TiposPago> tiposPago) {
		super();
		this.tiposPago = tiposPago;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public ConfiguradorRespuesta() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad tiposPago
	 *
	 * @return the tiposPago
	 */
	public List<CAT_TiposPago> getTiposPago() {
		return tiposPago;
	}

	/**
	 * Método para establecer el valor de una propiedad tiposPago
	 *
	 * @param tiposPago the tiposPago to set
	 */
	public void setTiposPago(List<CAT_TiposPago> tiposPago) {
		this.tiposPago = tiposPago;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfiguradorRespuesta [tiposPago=");
		builder.append(tiposPago);
		builder.append("]");
		return builder.toString();
	}
	
	
}

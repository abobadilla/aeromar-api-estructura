package aeromar.api.pasarela.models;

import org.springframework.web.bind.annotation.RequestBody;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 24 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
public class RequestPagoReferenciado {
	private String solicitud;
	private String afiliacion;
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param solicitud
	 * @param afiliacion
	 */
	public RequestPagoReferenciado(String solicitud, String afiliacion) {
		super();
		this.solicitud = solicitud;
		this.afiliacion = afiliacion;
	}
	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public RequestPagoReferenciado() {
		super();
	}
	/**
	 * Método para recuperar el valor de la propiedad solicitud
	 *
	 * @return the solicitud
	 */
	public String getSolicitud() {
		return solicitud;
	}
	/**
	 * Método para establecer el valor de una propiedad solicitud
	 *
	 * @param solicitud the solicitud to set
	 */
	public void setSolicitud(String solicitud) {
		this.solicitud = solicitud;
	}
	/**
	 * Método para recuperar el valor de la propiedad afiliacion
	 *
	 * @return the afiliacion
	 */
	public String getAfiliacion() {
		return afiliacion;
	}
	/**
	 * Método para establecer el valor de una propiedad afiliacion
	 *
	 * @param afiliacion the afiliacion to set
	 */
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequestPagoReferenciado [solicitud=");
		builder.append(solicitud);
		builder.append(", afiliacion=");
		builder.append(afiliacion);
		builder.append("]");
		return builder.toString();
	}
	
}

package aeromar.api.pasarela.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import aeromar.api.configuration.models.BitacoraRegistro;


/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 2 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Entity
@Table(name = "PP_CAT_UnidadDeNegocio", indexes = { @Index(name = "IDX_PP_CLAVE", columnList = "CLAVE", unique = true) })
public class CAT_UnidadDeNegocio extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_UnidadNegocio", nullable = false)
	private Integer idUnidadNegocio;
	
	@Column(name = "CLAVE", length = 50)
	private String clave;
	
	@Column(name = "ETIQUETA", length = 70)
	private String etiqueta;

	@Column(name = "ESTADO")
	private Boolean estado;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idUnidadNegocio
	 * @param clave
	 * @param etiqueta
	 * @param estado
	 */
	public CAT_UnidadDeNegocio(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idUnidadNegocio, String clave, String etiqueta, Boolean estado) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idUnidadNegocio = idUnidadNegocio;
		this.clave = clave;
		this.etiqueta = etiqueta;
		this.estado = estado;
	}

	

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public CAT_UnidadDeNegocio() {
		super();
	}



	/**
	 * Método para recuperar el valor de la propiedad idUnidadNegocio
	 *
	 * @return the idUnidadNegocio
	 */
	public Integer getIdUnidadNegocio() {
		return idUnidadNegocio;
	}

	/**
	 * Método para establecer el valor de una propiedad idUnidadNegocio
	 *
	 * @param idUnidadNegocio the idUnidadNegocio to set
	 */
	public void setIdUnidadNegocio(Integer idUnidadNegocio) {
		this.idUnidadNegocio = idUnidadNegocio;
	}

	/**
	 * Método para recuperar el valor de la propiedad clave
	 *
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Método para establecer el valor de una propiedad clave
	 *
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * Método para recuperar el valor de la propiedad etiqueta
	 *
	 * @return the etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad etiqueta
	 *
	 * @param etiqueta the etiqueta to set
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad estado
	 *
	 * @return the estado
	 */
	public Boolean getEstado() {
		return estado;
	}

	/**
	 * Método para establecer el valor de una propiedad estado
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CAT_UnidadDeNegocio [idUnidadNegocio=");
		builder.append(idUnidadNegocio);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", etiqueta=");
		builder.append(etiqueta);
		builder.append(", estado=");
		builder.append(estado);
		builder.append("]");
		return builder.toString();
	}

	

}

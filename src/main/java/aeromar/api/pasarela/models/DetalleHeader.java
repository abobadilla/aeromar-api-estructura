package aeromar.api.pasarela.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import aeromar.api.configuration.models.BitacoraRegistro;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 2 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Entity
@Table(name = "PP_Detalle_Header")
public class DetalleHeader extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_DetalleHeader", nullable = false)
	private Integer idDetalleHeader;
	
	@JsonBackReference
	@ManyToOne
    @JoinColumn(name = "FK_ID_Header")
    private Header fkIdHeader;
	
	
	@Column(name = "REFERENCIA", length = 70)
	private String referencia;
	
	@Column(name = "MONTO_BOLETO")
	private BigDecimal montoBoleto;
	
	@Column(name = "DESCRIPCION", columnDefinition="TEXT")
	private String descripcion;
	
	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ID_UnidadNegocio", referencedColumnName  = "ID_UnidadNegocio", nullable = false)
	private CAT_UnidadDeNegocio fkIdUnidadNegocio;
	
	
	@OneToMany(cascade =  CascadeType.ALL , mappedBy = "fkIdDetalleHeader")
	private List<AgrupadorDetalle> cupones;
	
	@Column(name = "ESTATUS")
	private Boolean estatus;
	
	@Column(name = "ESTATUS_REEMBOLSO")
	private Boolean estatusReembolso;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idDetalleHeader
	 * @param fkIdHeader
	 * @param referencia
	 * @param montoBoleto
	 * @param descripcion
	 * @param fkIdUnidadNegocio
	 * @param cupones
	 * @param estatus
	 * @param estatusReembolso
	 */
	public DetalleHeader(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idDetalleHeader, Header fkIdHeader, String referencia, BigDecimal montoBoleto, String descripcion,
			CAT_UnidadDeNegocio fkIdUnidadNegocio, List<AgrupadorDetalle> cupones, Boolean estatus,
			Boolean estatusReembolso) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idDetalleHeader = idDetalleHeader;
		this.fkIdHeader = fkIdHeader;
		this.referencia = referencia;
		this.montoBoleto = montoBoleto;
		this.descripcion = descripcion;
		this.fkIdUnidadNegocio = fkIdUnidadNegocio;
		this.cupones = cupones;
		this.estatus = estatus;
		this.estatusReembolso = estatusReembolso;
	}

	

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public DetalleHeader() {
		super();
	}



	/**
	 * Método para recuperar el valor de la propiedad idDetalleHeader
	 *
	 * @return the idDetalleHeader
	 */
	public Integer getIdDetalleHeader() {
		return idDetalleHeader;
	}

	/**
	 * Método para establecer el valor de una propiedad idDetalleHeader
	 *
	 * @param idDetalleHeader the idDetalleHeader to set
	 */
	public void setIdDetalleHeader(Integer idDetalleHeader) {
		this.idDetalleHeader = idDetalleHeader;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdHeader
	 *
	 * @return the fkIdHeader
	 */
	public Header getFkIdHeader() {
		return fkIdHeader;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdHeader
	 *
	 * @param fkIdHeader the fkIdHeader to set
	 */
	public void setFkIdHeader(Header fkIdHeader) {
		this.fkIdHeader = fkIdHeader;
	}

	/**
	 * Método para recuperar el valor de la propiedad referencia
	 *
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * Método para establecer el valor de una propiedad referencia
	 *
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * Método para recuperar el valor de la propiedad montoBoleto
	 *
	 * @return the montoBoleto
	 */
	public BigDecimal getMontoBoleto() {
		return montoBoleto;
	}

	/**
	 * Método para establecer el valor de una propiedad montoBoleto
	 *
	 * @param montoBoleto the montoBoleto to set
	 */
	public void setMontoBoleto(BigDecimal montoBoleto) {
		this.montoBoleto = montoBoleto;
	}

	/**
	 * Método para recuperar el valor de la propiedad descripcion
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Método para establecer el valor de una propiedad descripcion
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdUnidadNegocio
	 *
	 * @return the fkIdUnidadNegocio
	 */
	public CAT_UnidadDeNegocio getFkIdUnidadNegocio() {
		return fkIdUnidadNegocio;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdUnidadNegocio
	 *
	 * @param fkIdUnidadNegocio the fkIdUnidadNegocio to set
	 */
	public void setFkIdUnidadNegocio(CAT_UnidadDeNegocio fkIdUnidadNegocio) {
		this.fkIdUnidadNegocio = fkIdUnidadNegocio;
	}

	/**
	 * Método para recuperar el valor de la propiedad cupones
	 *
	 * @return the cupones
	 */
	public List<AgrupadorDetalle> getCupones() {
		return cupones;
	}

	/**
	 * Método para establecer el valor de una propiedad cupones
	 *
	 * @param cupones the cupones to set
	 */
	public void setCupones(List<AgrupadorDetalle> cupones) {
		this.cupones = cupones;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatus
	 *
	 * @return the estatus
	 */
	public Boolean getEstatus() {
		return estatus;
	}

	/**
	 * Método para establecer el valor de una propiedad estatus
	 *
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatusReembolso
	 *
	 * @return the estatusReembolso
	 */
	public Boolean getEstatusReembolso() {
		return estatusReembolso;
	}

	/**
	 * Método para establecer el valor de una propiedad estatusReembolso
	 *
	 * @param estatusReembolso the estatusReembolso to set
	 */
	public void setEstatusReembolso(Boolean estatusReembolso) {
		this.estatusReembolso = estatusReembolso;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DetalleHeader [idDetalleHeader=");
		builder.append(idDetalleHeader);
		builder.append(", fkIdHeader=");
		builder.append(fkIdHeader);
		builder.append(", referencia=");
		builder.append(referencia);
		builder.append(", montoBoleto=");
		builder.append(montoBoleto);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(", fkIdUnidadNegocio=");
		builder.append(fkIdUnidadNegocio);
		builder.append(", cupones=");
		builder.append(cupones);
		builder.append(", estatus=");
		builder.append(estatus);
		builder.append(", estatusReembolso=");
		builder.append(estatusReembolso);
		builder.append("]");
		return builder.toString();
	}
	
	

}

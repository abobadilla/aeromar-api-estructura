package aeromar.api.pasarela.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import aeromar.api.configuration.models.BitacoraRegistro;


/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 2 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Entity
@Table(name = "PP_CAT_TiposPago", indexes = { @Index(name = "IDX_PP_CLAVE", columnList = "CLAVE", unique = true) })
public class CAT_TiposPago extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TipoPago", nullable = false)
	private Integer idTipoPago;
	
	@Column(name = "CLAVE", length = 50)
	private String clave;
	
	@Column(name = "ETIQUETA", length = 70)
	private String etiqueta;

	@Column(name = "ESTADO")
	private Boolean estado;
	
	@Column(name = "CLAVE_AMADEUS", length = 80)
	private String claveAmadeus;
	
	@JsonManagedReference
	@OneToMany(cascade =  CascadeType.ALL , mappedBy = "tipoPago")
	private List<CAT_TiposTarjetas> tipoTarjeta = new ArrayList<CAT_TiposTarjetas>();

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param idTipoPago
	 * @param clave
	 * @param etiqueta
	 * @param estado
	 * @param claveAmadeus
	 * @param tipoTarjeta
	 */
	public CAT_TiposPago(Integer idTipoPago, String clave, String etiqueta, Boolean estado, String claveAmadeus,
			List<CAT_TiposTarjetas> tipoTarjeta) {
		super();
		this.idTipoPago = idTipoPago;
		this.clave = clave;
		this.etiqueta = etiqueta;
		this.estado = estado;
		this.claveAmadeus = claveAmadeus;
		this.tipoTarjeta = tipoTarjeta;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public CAT_TiposPago() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad idTipoPago
	 *
	 * @return the idTipoPago
	 */
	public Integer getIdTipoPago() {
		return idTipoPago;
	}

	/**
	 * Método para establecer el valor de una propiedad idTipoPago
	 *
	 * @param idTipoPago the idTipoPago to set
	 */
	public void setIdTipoPago(Integer idTipoPago) {
		this.idTipoPago = idTipoPago;
	}

	/**
	 * Método para recuperar el valor de la propiedad clave
	 *
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Método para establecer el valor de una propiedad clave
	 *
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * Método para recuperar el valor de la propiedad etiqueta
	 *
	 * @return the etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad etiqueta
	 *
	 * @param etiqueta the etiqueta to set
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad estado
	 *
	 * @return the estado
	 */
	public Boolean getEstado() {
		return estado;
	}

	/**
	 * Método para establecer el valor de una propiedad estado
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	/**
	 * Método para recuperar el valor de la propiedad claveAmadeus
	 *
	 * @return the claveAmadeus
	 */
	public String getClaveAmadeus() {
		return claveAmadeus;
	}

	/**
	 * Método para establecer el valor de una propiedad claveAmadeus
	 *
	 * @param claveAmadeus the claveAmadeus to set
	 */
	public void setClaveAmadeus(String claveAmadeus) {
		this.claveAmadeus = claveAmadeus;
	}

	/**
	 * Método para recuperar el valor de la propiedad tipoTarjeta
	 *
	 * @return the tipoTarjeta
	 */
	public List<CAT_TiposTarjetas> getTipoTarjeta() {
		return tipoTarjeta;
	}

	/**
	 * Método para establecer el valor de una propiedad tipoTarjeta
	 *
	 * @param tipoTarjeta the tipoTarjeta to set
	 */
	public void setTipoTarjeta(List<CAT_TiposTarjetas> tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CAT_TiposPago [idTipoPago=");
		builder.append(idTipoPago);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", etiqueta=");
		builder.append(etiqueta);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", claveAmadeus=");
		builder.append(claveAmadeus);
		builder.append(", tipoTarjeta=");
		builder.append(tipoTarjeta);
		builder.append("]");
		return builder.toString();
	}


}

package aeromar.api.pasarela.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import aeromar.api.configuration.models.BitacoraRegistro;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 2 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Entity
@Table(name = "PP_Etiquetas", indexes = { @Index(name = "IDX_PP_CLAVEETIQUETA", columnList = "CLAVEETIQUETA", unique = true) })
public class Etiquetas extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_Etiqueta", nullable = false)
	private Integer idEtiqueta;
	
	@Column(name = "TIPOETIQUETA", length = 30)
	private String tipoEtiqueta;
	
	@Column(name = "CLAVEETIQUETA", length = 100)
	private String claveEtiqueta;
	
	@Column(name = "DESCRIPCION", columnDefinition="TEXT")
	private String descripcion;
	
	
	@Column(name = "FECHAINICIO")
	@Temporal(TemporalType.DATE)
	private Date fecInicio;
	
	@Column(name = "FECHAFIN")
	@Temporal(TemporalType.DATE)
	private Date fecFin;
	
	@Column(name = "ORDENDESPLIEGUE")
	private Integer ordenDespliegue;
	
	@Column(name = "ESTATUS")
	private Boolean estatus;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idEtiqueta
	 * @param tipoEtiqueta
	 * @param claveEtiqueta
	 * @param descripcion
	 * @param fecInicio
	 * @param fecFin
	 * @param ordenDespliegue
	 * @param estatus
	 */
	public Etiquetas(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idEtiqueta, String tipoEtiqueta, String claveEtiqueta, String descripcion, Date fecInicio,
			Date fecFin, Integer ordenDespliegue, Boolean estatus) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idEtiqueta = idEtiqueta;
		this.tipoEtiqueta = tipoEtiqueta;
		this.claveEtiqueta = claveEtiqueta;
		this.descripcion = descripcion;
		this.fecInicio = fecInicio;
		this.fecFin = fecFin;
		this.ordenDespliegue = ordenDespliegue;
		this.estatus = estatus;
	}

	

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public Etiquetas() {
		super();
	}



	/**
	 * Método para recuperar el valor de la propiedad idEtiqueta
	 *
	 * @return the idEtiqueta
	 */
	public Integer getIdEtiqueta() {
		return idEtiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad idEtiqueta
	 *
	 * @param idEtiqueta the idEtiqueta to set
	 */
	public void setIdEtiqueta(Integer idEtiqueta) {
		this.idEtiqueta = idEtiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad tipoEtiqueta
	 *
	 * @return the tipoEtiqueta
	 */
	public String getTipoEtiqueta() {
		return tipoEtiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad tipoEtiqueta
	 *
	 * @param tipoEtiqueta the tipoEtiqueta to set
	 */
	public void setTipoEtiqueta(String tipoEtiqueta) {
		this.tipoEtiqueta = tipoEtiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad claveEtiqueta
	 *
	 * @return the claveEtiqueta
	 */
	public String getClaveEtiqueta() {
		return claveEtiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad claveEtiqueta
	 *
	 * @param claveEtiqueta the claveEtiqueta to set
	 */
	public void setClaveEtiqueta(String claveEtiqueta) {
		this.claveEtiqueta = claveEtiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad descripcion
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Método para establecer el valor de una propiedad descripcion
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Método para recuperar el valor de la propiedad fecInicio
	 *
	 * @return the fecInicio
	 */
	public Date getFecInicio() {
		return fecInicio;
	}

	/**
	 * Método para establecer el valor de una propiedad fecInicio
	 *
	 * @param fecInicio the fecInicio to set
	 */
	public void setFecInicio(Date fecInicio) {
		this.fecInicio = fecInicio;
	}

	/**
	 * Método para recuperar el valor de la propiedad fecFin
	 *
	 * @return the fecFin
	 */
	public Date getFecFin() {
		return fecFin;
	}

	/**
	 * Método para establecer el valor de una propiedad fecFin
	 *
	 * @param fecFin the fecFin to set
	 */
	public void setFecFin(Date fecFin) {
		this.fecFin = fecFin;
	}

	/**
	 * Método para recuperar el valor de la propiedad ordenDespliegue
	 *
	 * @return the ordenDespliegue
	 */
	public Integer getOrdenDespliegue() {
		return ordenDespliegue;
	}

	/**
	 * Método para establecer el valor de una propiedad ordenDespliegue
	 *
	 * @param ordenDespliegue the ordenDespliegue to set
	 */
	public void setOrdenDespliegue(Integer ordenDespliegue) {
		this.ordenDespliegue = ordenDespliegue;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatus
	 *
	 * @return the estatus
	 */
	public Boolean getEstatus() {
		return estatus;
	}

	/**
	 * Método para establecer el valor de una propiedad estatus
	 *
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Etiquetas [idEtiqueta=");
		builder.append(idEtiqueta);
		builder.append(", tipoEtiqueta=");
		builder.append(tipoEtiqueta);
		builder.append(", claveEtiqueta=");
		builder.append(claveEtiqueta);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(", fecInicio=");
		builder.append(fecInicio);
		builder.append(", fecFin=");
		builder.append(fecFin);
		builder.append(", ordenDespliegue=");
		builder.append(ordenDespliegue);
		builder.append(", estatus=");
		builder.append(estatus);
		builder.append("]");
		return builder.toString();
	}
	
	
}

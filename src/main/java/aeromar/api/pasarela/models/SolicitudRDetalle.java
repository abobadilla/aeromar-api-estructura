package aeromar.api.pasarela.models;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import aeromar.api.configuration.models.BitacoraRegistro;



@Entity
@Table(name = "PP_SolicitudRDetalle")
public class SolicitudRDetalle extends BitacoraRegistro{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SolicitudRDetalle", nullable = false)
	private Integer idSolicitudRDetalle;
	
	@Column(name = "MONTO_SOLICITUD")
	private BigDecimal montoSolicitud;
	
	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ID_DetalleHeader", referencedColumnName  = "ID_DetalleHeader")
	private DetalleHeader fkIdDetalleHeader;
	
	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ID_AgrupadorDetalle", referencedColumnName  = "ID_AgrupadorDetalle")
	private AgrupadorDetalle fkIdAgrupadorDetalle;
	
	@Column(name = "FECHA")
	@Temporal(TemporalType.DATE)
	private Date fecNac;
	
	/*
	 * 1: Unidad de negocios
	 * 2: Pagos
	 */
	@Column(name = "TIPO")
	private Integer tipo;
	
	
	@ManyToOne
    @JoinColumn(name = "FK_ID_SolicitudReembolso", nullable = false)
    private SolicitudReembolso fkIdSolicitudReembolso;


	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idSolicitudRDetalle
	 * @param montoSolicitud
	 * @param fkIdDetalleHeader
	 * @param fkIdAgrupadorDetalle
	 * @param fecNac
	 * @param tipo
	 * @param fkIdSolicitudReembolso
	 */
	public SolicitudRDetalle(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idSolicitudRDetalle, BigDecimal montoSolicitud, DetalleHeader fkIdDetalleHeader,
			AgrupadorDetalle fkIdAgrupadorDetalle, Date fecNac, Integer tipo,
			SolicitudReembolso fkIdSolicitudReembolso) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idSolicitudRDetalle = idSolicitudRDetalle;
		this.montoSolicitud = montoSolicitud;
		this.fkIdDetalleHeader = fkIdDetalleHeader;
		this.fkIdAgrupadorDetalle = fkIdAgrupadorDetalle;
		this.fecNac = fecNac;
		this.tipo = tipo;
		this.fkIdSolicitudReembolso = fkIdSolicitudReembolso;
	}




	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public SolicitudRDetalle() {
		super();
	}




	/**
	 * Método para recuperar el valor de la propiedad idSolicitudRDetalle
	 *
	 * @return the idSolicitudRDetalle
	 */
	public Integer getIdSolicitudRDetalle() {
		return idSolicitudRDetalle;
	}


	/**
	 * Método para establecer el valor de una propiedad idSolicitudRDetalle
	 *
	 * @param idSolicitudRDetalle the idSolicitudRDetalle to set
	 */
	public void setIdSolicitudRDetalle(Integer idSolicitudRDetalle) {
		this.idSolicitudRDetalle = idSolicitudRDetalle;
	}


	/**
	 * Método para recuperar el valor de la propiedad montoSolicitud
	 *
	 * @return the montoSolicitud
	 */
	public BigDecimal getMontoSolicitud() {
		return montoSolicitud;
	}


	/**
	 * Método para establecer el valor de una propiedad montoSolicitud
	 *
	 * @param montoSolicitud the montoSolicitud to set
	 */
	public void setMontoSolicitud(BigDecimal montoSolicitud) {
		this.montoSolicitud = montoSolicitud;
	}


	/**
	 * Método para recuperar el valor de la propiedad fkIdDetalleHeader
	 *
	 * @return the fkIdDetalleHeader
	 */
	public DetalleHeader getFkIdDetalleHeader() {
		return fkIdDetalleHeader;
	}


	/**
	 * Método para establecer el valor de una propiedad fkIdDetalleHeader
	 *
	 * @param fkIdDetalleHeader the fkIdDetalleHeader to set
	 */
	public void setFkIdDetalleHeader(DetalleHeader fkIdDetalleHeader) {
		this.fkIdDetalleHeader = fkIdDetalleHeader;
	}


	/**
	 * Método para recuperar el valor de la propiedad fkIdAgrupadorDetalle
	 *
	 * @return the fkIdAgrupadorDetalle
	 */
	public AgrupadorDetalle getFkIdAgrupadorDetalle() {
		return fkIdAgrupadorDetalle;
	}


	/**
	 * Método para establecer el valor de una propiedad fkIdAgrupadorDetalle
	 *
	 * @param fkIdAgrupadorDetalle the fkIdAgrupadorDetalle to set
	 */
	public void setFkIdAgrupadorDetalle(AgrupadorDetalle fkIdAgrupadorDetalle) {
		this.fkIdAgrupadorDetalle = fkIdAgrupadorDetalle;
	}


	/**
	 * Método para recuperar el valor de la propiedad fecNac
	 *
	 * @return the fecNac
	 */
	public Date getFecNac() {
		return fecNac;
	}


	/**
	 * Método para establecer el valor de una propiedad fecNac
	 *
	 * @param fecNac the fecNac to set
	 */
	public void setFecNac(Date fecNac) {
		this.fecNac = fecNac;
	}


	/**
	 * Método para recuperar el valor de la propiedad tipo
	 *
	 * @return the tipo
	 */
	public Integer getTipo() {
		return tipo;
	}


	/**
	 * Método para establecer el valor de una propiedad tipo
	 *
	 * @param tipo the tipo to set
	 */
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}


	/**
	 * Método para recuperar el valor de la propiedad fkIdSolicitudReembolso
	 *
	 * @return the fkIdSolicitudReembolso
	 */
	public SolicitudReembolso getFkIdSolicitudReembolso() {
		return fkIdSolicitudReembolso;
	}


	/**
	 * Método para establecer el valor de una propiedad fkIdSolicitudReembolso
	 *
	 * @param fkIdSolicitudReembolso the fkIdSolicitudReembolso to set
	 */
	public void setFkIdSolicitudReembolso(SolicitudReembolso fkIdSolicitudReembolso) {
		this.fkIdSolicitudReembolso = fkIdSolicitudReembolso;
	}


	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SolicitudRDetalle [idSolicitudRDetalle=");
		builder.append(idSolicitudRDetalle);
		builder.append(", montoSolicitud=");
		builder.append(montoSolicitud);
		builder.append(", fkIdDetalleHeader=");
		builder.append(fkIdDetalleHeader);
		builder.append(", fkIdAgrupadorDetalle=");
		builder.append(fkIdAgrupadorDetalle);
		builder.append(", fecNac=");
		builder.append(fecNac);
		builder.append(", tipo=");
		builder.append(tipo);
		builder.append(", fkIdSolicitudReembolso=");
		builder.append(fkIdSolicitudReembolso);
		builder.append("]");
		return builder.toString();
	}
	

	
	
}

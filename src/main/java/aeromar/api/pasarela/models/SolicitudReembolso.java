package aeromar.api.pasarela.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import aeromar.api.configuration.models.BitacoraRegistro;



@Entity
@Table(name = "PP_SolicitudReembolso")
public class SolicitudReembolso extends BitacoraRegistro{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SolicitudReembolso", nullable = false)
	private Integer idSolicitudReembolso;
	
	@Column(name = "MONTO_TOTAL")
	private BigDecimal montoTotal;
	
	@Column(name = "FECHA")
	@Temporal(TemporalType.DATE)
	private Date fecNac;
	
	@Column(name = "DESCRIPCION", columnDefinition="TEXT")
	private String descripcion;
	
	// Estatus de la solicitud
	@Column(name = "ESTATUS", length = 70, nullable = false)
	private String estatus;
	
	@OneToMany(mappedBy="fkIdSolicitudReembolso")
    private List<SolicitudRDetalle> solicitudesRDetalles;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idSolicitudReembolso
	 * @param montoTotal
	 * @param fecNac
	 * @param descripcion
	 * @param estatus
	 * @param solicitudesRDetalles
	 */
	public SolicitudReembolso(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idSolicitudReembolso, BigDecimal montoTotal, Date fecNac, String descripcion, String estatus,
			List<SolicitudRDetalle> solicitudesRDetalles) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idSolicitudReembolso = idSolicitudReembolso;
		this.montoTotal = montoTotal;
		this.fecNac = fecNac;
		this.descripcion = descripcion;
		this.estatus = estatus;
		this.solicitudesRDetalles = solicitudesRDetalles;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public SolicitudReembolso() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad idSolicitudReembolso
	 *
	 * @return the idSolicitudReembolso
	 */
	public Integer getIdSolicitudReembolso() {
		return idSolicitudReembolso;
	}

	/**
	 * Método para establecer el valor de una propiedad idSolicitudReembolso
	 *
	 * @param idSolicitudReembolso the idSolicitudReembolso to set
	 */
	public void setIdSolicitudReembolso(Integer idSolicitudReembolso) {
		this.idSolicitudReembolso = idSolicitudReembolso;
	}

	/**
	 * Método para recuperar el valor de la propiedad montoTotal
	 *
	 * @return the montoTotal
	 */
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	/**
	 * Método para establecer el valor de una propiedad montoTotal
	 *
	 * @param montoTotal the montoTotal to set
	 */
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * Método para recuperar el valor de la propiedad fecNac
	 *
	 * @return the fecNac
	 */
	public Date getFecNac() {
		return fecNac;
	}

	/**
	 * Método para establecer el valor de una propiedad fecNac
	 *
	 * @param fecNac the fecNac to set
	 */
	public void setFecNac(Date fecNac) {
		this.fecNac = fecNac;
	}

	/**
	 * Método para recuperar el valor de la propiedad descripcion
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Método para establecer el valor de una propiedad descripcion
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatus
	 *
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Método para establecer el valor de una propiedad estatus
	 *
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Método para recuperar el valor de la propiedad solicitudesRDetalles
	 *
	 * @return the solicitudesRDetalles
	 */
	public List<SolicitudRDetalle> getSolicitudesRDetalles() {
		return solicitudesRDetalles;
	}

	/**
	 * Método para establecer el valor de una propiedad solicitudesRDetalles
	 *
	 * @param solicitudesRDetalles the solicitudesRDetalles to set
	 */
	public void setSolicitudesRDetalles(List<SolicitudRDetalle> solicitudesRDetalles) {
		this.solicitudesRDetalles = solicitudesRDetalles;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SolicitudReembolso [idSolicitudReembolso=");
		builder.append(idSolicitudReembolso);
		builder.append(", montoTotal=");
		builder.append(montoTotal);
		builder.append(", fecNac=");
		builder.append(fecNac);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(", estatus=");
		builder.append(estatus);
		builder.append(", solicitudesRDetalles=");
		builder.append(solicitudesRDetalles);
		builder.append("]");
		return builder.toString();
	}
	
	
    
}

package aeromar.api.pasarela.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import aeromar.api.configuration.models.BitacoraRegistro;


@Entity
@Table(name = "PP_ValorEtiqueta")
public class ValorEtiqueta extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ValorEtiqueta", nullable = false)
	private Integer idValorEtiqueta;
	
	@JsonBackReference
	@ManyToOne
    @JoinColumn(name="FK_ID_AgrupadorDetalle", nullable=false)
    private AgrupadorDetalle fkIdAgrupadorDetalle;


	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ID_Etiqueta", referencedColumnName  = "ID_Etiqueta", nullable = false)
	private Etiquetas fkIdEtiqueta;
	
	
	@Column(name = "VALOR")
	private String valor;
	
	@Column(name = "ESTATUS")
	private Boolean estatus;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idValorEtiqueta
	 * @param fkIdAgrupadorDetalle
	 * @param fkIdEtiqueta
	 * @param valor
	 * @param estatus
	 */
	public ValorEtiqueta(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate,
			Integer idValorEtiqueta, AgrupadorDetalle fkIdAgrupadorDetalle, Etiquetas fkIdEtiqueta, String valor,
			Boolean estatus) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idValorEtiqueta = idValorEtiqueta;
		this.fkIdAgrupadorDetalle = fkIdAgrupadorDetalle;
		this.fkIdEtiqueta = fkIdEtiqueta;
		this.valor = valor;
		this.estatus = estatus;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public ValorEtiqueta() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad idValorEtiqueta
	 *
	 * @return the idValorEtiqueta
	 */
	public Integer getIdValorEtiqueta() {
		return idValorEtiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad idValorEtiqueta
	 *
	 * @param idValorEtiqueta the idValorEtiqueta to set
	 */
	public void setIdValorEtiqueta(Integer idValorEtiqueta) {
		this.idValorEtiqueta = idValorEtiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdAgrupadorDetalle
	 *
	 * @return the fkIdAgrupadorDetalle
	 */
	public AgrupadorDetalle getFkIdAgrupadorDetalle() {
		return fkIdAgrupadorDetalle;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdAgrupadorDetalle
	 *
	 * @param fkIdAgrupadorDetalle the fkIdAgrupadorDetalle to set
	 */
	public void setFkIdAgrupadorDetalle(AgrupadorDetalle fkIdAgrupadorDetalle) {
		this.fkIdAgrupadorDetalle = fkIdAgrupadorDetalle;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdEtiqueta
	 *
	 * @return the fkIdEtiqueta
	 */
	public Etiquetas getFkIdEtiqueta() {
		return fkIdEtiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdEtiqueta
	 *
	 * @param fkIdEtiqueta the fkIdEtiqueta to set
	 */
	public void setFkIdEtiqueta(Etiquetas fkIdEtiqueta) {
		this.fkIdEtiqueta = fkIdEtiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad valor
	 *
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Método para establecer el valor de una propiedad valor
	 *
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatus
	 *
	 * @return the estatus
	 */
	public Boolean getEstatus() {
		return estatus;
	}

	/**
	 * Método para establecer el valor de una propiedad estatus
	 *
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ValorEtiqueta [idValorEtiqueta=");
		builder.append(idValorEtiqueta);
		builder.append(", fkIdAgrupadorDetalle=");
		builder.append(fkIdAgrupadorDetalle);
		builder.append(", fkIdEtiqueta=");
		builder.append(fkIdEtiqueta);
		builder.append(", valor=");
		builder.append(valor);
		builder.append(", estatus=");
		builder.append(estatus);
		builder.append("]");
		return builder.toString();
	}
	
	

}

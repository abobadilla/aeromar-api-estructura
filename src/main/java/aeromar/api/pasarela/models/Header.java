package aeromar.api.pasarela.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import aeromar.api.configuration.models.BitacoraRegistro;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 2 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Entity
@Table(name = "PP_Header", indexes = { @Index(name = "IDX_PP_FOLIOREGISTRO", columnList = "FOLIOREGISTRO", unique = true) })
public class Header extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_Header", nullable = false)
	private Integer idHeader;
	
	@Column(name = "CLAVEREFERENCIA", length = 70, nullable = false)
	private String claveReferencia;
	
	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ID_SISTEMAORIGEN", referencedColumnName  = "ID_SistemaOrigen")
	private CAT_SistemaOrigen fkIdSistemaOrigen;
	
	@Column(name = "FOLIOREGISTRO", length = 100)
	private String folioRegistro;
	
	@Column(name = "ESTATUSCOBRO", length = 70)
	private String estatusCobro;
	
	@Column(name = "OFFICEID", length = 100, nullable = false)
	private String officeID;
	
	@Column(name = "IATA", length = 100)
	private String iata;
	
	@Column(name = "MONTOCOBRAR")
	private BigDecimal montoCobrar;
	
	@Column(name = "MONTOPENDIENTECOBRAR")
	private BigDecimal montoPendienteCobrar;
	
	@Column(name = "MONTOCOBRADO")
	private BigDecimal montoCobrado;
	
	@Column(name = "ESTATUSREEMBOLSO", length = 50, nullable = false)
	private String estatusReembolso;
	
	/**
	 * MXN
	 * USD
	 */
	@Column(name = "CLAVEMONEDA", length = 5, nullable = false)
	private String claveMoneda;
	
	@Column(name = "FK_ID_TIPOCARGA", length = 10)
	private Integer fkIdTipoCarga;
	
	@Column(name = "TELEFONO", nullable = false)
	private String telefono;
	
	@Column(name = "PROMOCODE", length = 30)
	private String promocode;
	
	@Column(name = "NOMBRECONTACTO" ,nullable = false)
	private String nombreContacto;
	
	@Column(name = "APELLIDOPCONTACTO", nullable = false)
	private String apellidoPContacto;
	
	@Column(name = "APELLIDOMCONTACTO", nullable = false)
	private String apellidoMContacto;
	
	@Column(name = "CORREOCONTACTO", nullable = false)
	private String correoContacto;
	
	@JsonManagedReference
	@OneToMany(cascade =  CascadeType.ALL , mappedBy = "fkIdHeader")
	private List<DetalleHeader> detallesHeader;
	
	@JsonManagedReference(value="user-payment")
	@OneToMany(cascade =  CascadeType.ALL , mappedBy = "fkIdHeader")
	private List<AgrupadorDetalle> pagos;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param usuarioIdAlta
	 * @param usuarioIdAct
	 * @param createdDate
	 * @param updatedDate
	 * @param idHeader
	 * @param claveReferencia
	 * @param fkIdSistemaOrigen
	 * @param folioRegistro
	 * @param estatusCobro
	 * @param officeID
	 * @param iata
	 * @param montoCobrar
	 * @param montoPendienteCobrar
	 * @param montoCobrado
	 * @param estatusReembolso
	 * @param claveMoneda
	 * @param fkIdTipoCarga
	 * @param telefono
	 * @param promocode
	 * @param nombreContacto
	 * @param apellidoPContacto
	 * @param apellidoMContacto
	 * @param correoContacto
	 * @param detallesHeader
	 * @param pagos
	 */
	public Header(Integer usuarioIdAlta, Integer usuarioIdAct, Date createdDate, Date updatedDate, Integer idHeader,
			String claveReferencia, CAT_SistemaOrigen fkIdSistemaOrigen, String folioRegistro, String estatusCobro,
			String officeID, String iata, BigDecimal montoCobrar, BigDecimal montoPendienteCobrar,
			BigDecimal montoCobrado, String estatusReembolso, String claveMoneda, Integer fkIdTipoCarga,
			String telefono, String promocode, String nombreContacto, String apellidoPContacto,
			String apellidoMContacto, String correoContacto, List<DetalleHeader> detallesHeader,
			List<AgrupadorDetalle> pagos) {
		super(usuarioIdAlta, usuarioIdAct, createdDate, updatedDate);
		this.idHeader = idHeader;
		this.claveReferencia = claveReferencia;
		this.fkIdSistemaOrigen = fkIdSistemaOrigen;
		this.folioRegistro = folioRegistro;
		this.estatusCobro = estatusCobro;
		this.officeID = officeID;
		this.iata = iata;
		this.montoCobrar = montoCobrar;
		this.montoPendienteCobrar = montoPendienteCobrar;
		this.montoCobrado = montoCobrado;
		this.estatusReembolso = estatusReembolso;
		this.claveMoneda = claveMoneda;
		this.fkIdTipoCarga = fkIdTipoCarga;
		this.telefono = telefono;
		this.promocode = promocode;
		this.nombreContacto = nombreContacto;
		this.apellidoPContacto = apellidoPContacto;
		this.apellidoMContacto = apellidoMContacto;
		this.correoContacto = correoContacto;
		this.detallesHeader = detallesHeader;
		this.pagos = pagos;
	}

	

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public Header() {
		super();
	}



	/**
	 * Método para recuperar el valor de la propiedad idHeader
	 *
	 * @return the idHeader
	 */
	public Integer getIdHeader() {
		return idHeader;
	}

	/**
	 * Método para establecer el valor de una propiedad idHeader
	 *
	 * @param idHeader the idHeader to set
	 */
	public void setIdHeader(Integer idHeader) {
		this.idHeader = idHeader;
	}

	/**
	 * Método para recuperar el valor de la propiedad claveReferencia
	 *
	 * @return the claveReferencia
	 */
	public String getClaveReferencia() {
		return claveReferencia;
	}

	/**
	 * Método para establecer el valor de una propiedad claveReferencia
	 *
	 * @param claveReferencia the claveReferencia to set
	 */
	public void setClaveReferencia(String claveReferencia) {
		this.claveReferencia = claveReferencia;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdSistemaOrigen
	 *
	 * @return the fkIdSistemaOrigen
	 */
	public CAT_SistemaOrigen getFkIdSistemaOrigen() {
		return fkIdSistemaOrigen;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdSistemaOrigen
	 *
	 * @param fkIdSistemaOrigen the fkIdSistemaOrigen to set
	 */
	public void setFkIdSistemaOrigen(CAT_SistemaOrigen fkIdSistemaOrigen) {
		this.fkIdSistemaOrigen = fkIdSistemaOrigen;
	}

	/**
	 * Método para recuperar el valor de la propiedad folioRegistro
	 *
	 * @return the folioRegistro
	 */
	public String getFolioRegistro() {
		return folioRegistro;
	}

	/**
	 * Método para establecer el valor de una propiedad folioRegistro
	 *
	 * @param folioRegistro the folioRegistro to set
	 */
	public void setFolioRegistro(String folioRegistro) {
		this.folioRegistro = folioRegistro;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatusCobro
	 *
	 * @return the estatusCobro
	 */
	public String getEstatusCobro() {
		return estatusCobro;
	}

	/**
	 * Método para establecer el valor de una propiedad estatusCobro
	 *
	 * @param estatusCobro the estatusCobro to set
	 */
	public void setEstatusCobro(String estatusCobro) {
		this.estatusCobro = estatusCobro;
	}

	/**
	 * Método para recuperar el valor de la propiedad officeID
	 *
	 * @return the officeID
	 */
	public String getOfficeID() {
		return officeID;
	}

	/**
	 * Método para establecer el valor de una propiedad officeID
	 *
	 * @param officeID the officeID to set
	 */
	public void setOfficeID(String officeID) {
		this.officeID = officeID;
	}

	/**
	 * Método para recuperar el valor de la propiedad iata
	 *
	 * @return the iata
	 */
	public String getIata() {
		return iata;
	}

	/**
	 * Método para establecer el valor de una propiedad iata
	 *
	 * @param iata the iata to set
	 */
	public void setIata(String iata) {
		this.iata = iata;
	}

	/**
	 * Método para recuperar el valor de la propiedad montoCobrar
	 *
	 * @return the montoCobrar
	 */
	public BigDecimal getMontoCobrar() {
		return montoCobrar;
	}

	/**
	 * Método para establecer el valor de una propiedad montoCobrar
	 *
	 * @param montoCobrar the montoCobrar to set
	 */
	public void setMontoCobrar(BigDecimal montoCobrar) {
		this.montoCobrar = montoCobrar;
	}

	/**
	 * Método para recuperar el valor de la propiedad montoPendienteCobrar
	 *
	 * @return the montoPendienteCobrar
	 */
	public BigDecimal getMontoPendienteCobrar() {
		return montoPendienteCobrar;
	}

	/**
	 * Método para establecer el valor de una propiedad montoPendienteCobrar
	 *
	 * @param montoPendienteCobrar the montoPendienteCobrar to set
	 */
	public void setMontoPendienteCobrar(BigDecimal montoPendienteCobrar) {
		this.montoPendienteCobrar = montoPendienteCobrar;
	}

	/**
	 * Método para recuperar el valor de la propiedad montoCobrado
	 *
	 * @return the montoCobrado
	 */
	public BigDecimal getMontoCobrado() {
		return montoCobrado;
	}

	/**
	 * Método para establecer el valor de una propiedad montoCobrado
	 *
	 * @param montoCobrado the montoCobrado to set
	 */
	public void setMontoCobrado(BigDecimal montoCobrado) {
		this.montoCobrado = montoCobrado;
	}

	/**
	 * Método para recuperar el valor de la propiedad estatusReembolso
	 *
	 * @return the estatusReembolso
	 */
	public String getEstatusReembolso() {
		return estatusReembolso;
	}

	/**
	 * Método para establecer el valor de una propiedad estatusReembolso
	 *
	 * @param estatusReembolso the estatusReembolso to set
	 */
	public void setEstatusReembolso(String estatusReembolso) {
		this.estatusReembolso = estatusReembolso;
	}

	/**
	 * Método para recuperar el valor de la propiedad claveMoneda
	 *
	 * @return the claveMoneda
	 */
	public String getClaveMoneda() {
		return claveMoneda;
	}

	/**
	 * Método para establecer el valor de una propiedad claveMoneda
	 *
	 * @param claveMoneda the claveMoneda to set
	 */
	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}

	/**
	 * Método para recuperar el valor de la propiedad fkIdTipoCarga
	 *
	 * @return the fkIdTipoCarga
	 */
	public Integer getFkIdTipoCarga() {
		return fkIdTipoCarga;
	}

	/**
	 * Método para establecer el valor de una propiedad fkIdTipoCarga
	 *
	 * @param fkIdTipoCarga the fkIdTipoCarga to set
	 */
	public void setFkIdTipoCarga(Integer fkIdTipoCarga) {
		this.fkIdTipoCarga = fkIdTipoCarga;
	}

	/**
	 * Método para recuperar el valor de la propiedad telefono
	 *
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * Método para establecer el valor de una propiedad telefono
	 *
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * Método para recuperar el valor de la propiedad promocode
	 *
	 * @return the promocode
	 */
	public String getPromocode() {
		return promocode;
	}

	/**
	 * Método para establecer el valor de una propiedad promocode
	 *
	 * @param promocode the promocode to set
	 */
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	/**
	 * Método para recuperar el valor de la propiedad nombreContacto
	 *
	 * @return the nombreContacto
	 */
	public String getNombreContacto() {
		return nombreContacto;
	}

	/**
	 * Método para establecer el valor de una propiedad nombreContacto
	 *
	 * @param nombreContacto the nombreContacto to set
	 */
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	/**
	 * Método para recuperar el valor de la propiedad apellidoPContacto
	 *
	 * @return the apellidoPContacto
	 */
	public String getApellidoPContacto() {
		return apellidoPContacto;
	}

	/**
	 * Método para establecer el valor de una propiedad apellidoPContacto
	 *
	 * @param apellidoPContacto the apellidoPContacto to set
	 */
	public void setApellidoPContacto(String apellidoPContacto) {
		this.apellidoPContacto = apellidoPContacto;
	}

	/**
	 * Método para recuperar el valor de la propiedad apellidoMContacto
	 *
	 * @return the apellidoMContacto
	 */
	public String getApellidoMContacto() {
		return apellidoMContacto;
	}

	/**
	 * Método para establecer el valor de una propiedad apellidoMContacto
	 *
	 * @param apellidoMContacto the apellidoMContacto to set
	 */
	public void setApellidoMContacto(String apellidoMContacto) {
		this.apellidoMContacto = apellidoMContacto;
	}

	/**
	 * Método para recuperar el valor de la propiedad correoContacto
	 *
	 * @return the correoContacto
	 */
	public String getCorreoContacto() {
		return correoContacto;
	}

	/**
	 * Método para establecer el valor de una propiedad correoContacto
	 *
	 * @param correoContacto the correoContacto to set
	 */
	public void setCorreoContacto(String correoContacto) {
		this.correoContacto = correoContacto;
	}

	/**
	 * Método para recuperar el valor de la propiedad detallesHeader
	 *
	 * @return the detallesHeader
	 */
	public List<DetalleHeader> getDetallesHeader() {
		return detallesHeader;
	}

	/**
	 * Método para establecer el valor de una propiedad detallesHeader
	 *
	 * @param detallesHeader the detallesHeader to set
	 */
	public void setDetallesHeader(List<DetalleHeader> detallesHeader) {
		this.detallesHeader = detallesHeader;
	}

	/**
	 * Método para recuperar el valor de la propiedad pagos
	 *
	 * @return the pagos
	 */
	public List<AgrupadorDetalle> getPagos() {
		return pagos;
	}

	/**
	 * Método para establecer el valor de una propiedad pagos
	 *
	 * @param pagos the pagos to set
	 */
	public void setPagos(List<AgrupadorDetalle> pagos) {
		this.pagos = pagos;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Header [idHeader=");
		builder.append(idHeader);
		builder.append(", claveReferencia=");
		builder.append(claveReferencia);
		builder.append(", fkIdSistemaOrigen=");
		builder.append(fkIdSistemaOrigen);
		builder.append(", folioRegistro=");
		builder.append(folioRegistro);
		builder.append(", estatusCobro=");
		builder.append(estatusCobro);
		builder.append(", officeID=");
		builder.append(officeID);
		builder.append(", iata=");
		builder.append(iata);
		builder.append(", montoCobrar=");
		builder.append(montoCobrar);
		builder.append(", montoPendienteCobrar=");
		builder.append(montoPendienteCobrar);
		builder.append(", montoCobrado=");
		builder.append(montoCobrado);
		builder.append(", estatusReembolso=");
		builder.append(estatusReembolso);
		builder.append(", claveMoneda=");
		builder.append(claveMoneda);
		builder.append(", fkIdTipoCarga=");
		builder.append(fkIdTipoCarga);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", promocode=");
		builder.append(promocode);
		builder.append(", nombreContacto=");
		builder.append(nombreContacto);
		builder.append(", apellidoPContacto=");
		builder.append(apellidoPContacto);
		builder.append(", apellidoMContacto=");
		builder.append(apellidoMContacto);
		builder.append(", correoContacto=");
		builder.append(correoContacto);
		builder.append(", detallesHeader=");
		builder.append(detallesHeader);
		builder.append(", pagos=");
		builder.append(pagos);
		builder.append("]");
		return builder.toString();
	}

	
	
}

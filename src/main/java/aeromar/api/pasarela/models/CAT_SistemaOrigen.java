package aeromar.api.pasarela.models;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import aeromar.api.configuration.models.BitacoraRegistro;

/**
 * 
 * @author desarrollo.4 (aeromar.com.mx)
 * @since 2 mar. 2021
 * @version 1.0.0
 *          <p>
 *          </p>
 */
@Entity
@Table(name = "PP_CAT_SistemaOrigen", indexes = { @Index(name = "IDX_PP_CLAVE", columnList = "CLAVE", unique = true) })
public class CAT_SistemaOrigen extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SistemaOrigen", nullable = false)
	private Integer idSistemaOrigen;
	
	@Column(name = "CLAVE", length = 50)
	private String clave;
	
	@Column(name = "ETIQUETA", length = 70)
	private String etiqueta;
	
	@Column(name = "ESTADO")
	private Boolean estado;

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param idSistemaOrigen
	 * @param clave
	 * @param etiqueta
	 * @param estado
	 */
	public CAT_SistemaOrigen(Integer idSistemaOrigen, String clave, String etiqueta, Boolean estado) {
		super();
		this.idSistemaOrigen = idSistemaOrigen;
		this.clave = clave;
		this.etiqueta = etiqueta;
		this.estado = estado;
	}

	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public CAT_SistemaOrigen() {
		super();
	}

	/**
	 * Método para recuperar el valor de la propiedad idSistemaOrigen
	 *
	 * @return the idSistemaOrigen
	 */
	public Integer getIdSistemaOrigen() {
		return idSistemaOrigen;
	}

	/**
	 * Método para establecer el valor de una propiedad idSistemaOrigen
	 *
	 * @param idSistemaOrigen the idSistemaOrigen to set
	 */
	public void setIdSistemaOrigen(Integer idSistemaOrigen) {
		this.idSistemaOrigen = idSistemaOrigen;
	}

	/**
	 * Método para recuperar el valor de la propiedad clave
	 *
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Método para establecer el valor de una propiedad clave
	 *
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * Método para recuperar el valor de la propiedad etiqueta
	 *
	 * @return the etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * Método para establecer el valor de una propiedad etiqueta
	 *
	 * @param etiqueta the etiqueta to set
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * Método para recuperar el valor de la propiedad estado
	 *
	 * @return the estado
	 */
	public Boolean getEstado() {
		return estado;
	}

	/**
	 * Método para establecer el valor de una propiedad estado
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CAT_SistemaOrigen [idSistemaOrigen=");
		builder.append(idSistemaOrigen);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", etiqueta=");
		builder.append(etiqueta);
		builder.append(", estado=");
		builder.append(estado);
		builder.append("]");
		return builder.toString();
	}



	
	
}

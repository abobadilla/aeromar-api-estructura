package aeromar.api.pasarela.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import aeromar.api.configuration.models.BitacoraRegistro;

@Entity
@Table(name = "PP_CAT_SO_FP")
public class CAT_SO_FP  extends BitacoraRegistro{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CAT_SO_FP", nullable = false)
	private Integer idSOFP;
	
	
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "FK_ID_SistemaOrigen", referencedColumnName  = "ID_SistemaOrigen", nullable = false)
	private CAT_SistemaOrigen fkSistemaOrigen;
	
	
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "FK_ID_TipoPago", referencedColumnName  = "ID_TipoPago", nullable = false)
	private CAT_TiposPago fkTipoPago;


	/**
	 * 
	 * Constructor de la clase.
	 * 
	 * @param idSOFP
	 * @param fkSistemaOrigen
	 * @param fkTipoPago
	 */
	public CAT_SO_FP(Integer idSOFP, CAT_SistemaOrigen fkSistemaOrigen, CAT_TiposPago fkTipoPago) {
		super();
		this.idSOFP = idSOFP;
		this.fkSistemaOrigen = fkSistemaOrigen;
		this.fkTipoPago = fkTipoPago;
	}


	/**
	 * 
	 * Constructor de la clase.
	 * 
	 */
	public CAT_SO_FP() {
		super();
	}


	/**
	 * Método para recuperar el valor de la propiedad idSOFP
	 *
	 * @return the idSOFP
	 */
	public Integer getIdSOFP() {
		return idSOFP;
	}


	/**
	 * Método para establecer el valor de una propiedad idSOFP
	 *
	 * @param idSOFP the idSOFP to set
	 */
	public void setIdSOFP(Integer idSOFP) {
		this.idSOFP = idSOFP;
	}


	/**
	 * Método para recuperar el valor de la propiedad fkSistemaOrigen
	 *
	 * @return the fkSistemaOrigen
	 */
	public CAT_SistemaOrigen getFkSistemaOrigen() {
		return fkSistemaOrigen;
	}


	/**
	 * Método para establecer el valor de una propiedad fkSistemaOrigen
	 *
	 * @param fkSistemaOrigen the fkSistemaOrigen to set
	 */
	public void setFkSistemaOrigen(CAT_SistemaOrigen fkSistemaOrigen) {
		this.fkSistemaOrigen = fkSistemaOrigen;
	}


	/**
	 * Método para recuperar el valor de la propiedad fkTipoPago
	 *
	 * @return the fkTipoPago
	 */
	public CAT_TiposPago getFkTipoPago() {
		return fkTipoPago;
	}


	/**
	 * Método para establecer el valor de una propiedad fkTipoPago
	 *
	 * @param fkTipoPago the fkTipoPago to set
	 */
	public void setFkTipoPago(CAT_TiposPago fkTipoPago) {
		this.fkTipoPago = fkTipoPago;
	}


	/**
	 *
	 * Descripción: 
	 *
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CAT_SO_FP [idSOFP=");
		builder.append(idSOFP);
		builder.append(", fkSistemaOrigen=");
		builder.append(fkSistemaOrigen);
		builder.append(", fkTipoPago=");
		builder.append(fkTipoPago);
		builder.append("]");
		return builder.toString();
	}

	
}
